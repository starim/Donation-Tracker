<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script contains the web-based user interface for managing the donation tracker.
 * 
 */


// ensures that the user visiting this page has logged in; if not, the user is booted to the login page
require_once('classes/session.php');
session::sessionStart();
require('web_interface_support/security.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Donation Manager</title>
	<meta charset="utf-8" />
	<!-- activates Chrome Frame for compatibility with older Internet Explorer browsers -->
	<!--[if lte IE 9]>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<![endif]-->
	<!-- for IE users that don't have Chrome Frame, try to support them with this HTML 5 shim -->
	<!--[if lte IE 8]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- <link rel="shortcut icon" type="image/x-icon" href="" /> -->
	<meta name="copyright" content="© 2009 Brent Houghton">
	
	<link rel="stylesheet" type="text/css" href="web_interface_support/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="web_interface_support/css/donation_manager_styles.css" />
	<link rel="stylesheet" type="text/css" href="web_interface_support/css/jquery-ui-cupertino.css">
	
	<script type="text/javascript" src="web_interface_support/javascript/jquery.js"></script>
	<script type="text/javascript" src="web_interface_support/javascript/jquery-ui.js"></script>
	<script type="text/javascript" src="web_interface_support/javascript/donation_manager_scripts.js"></script>
	<script type="text/javascript">
	
	var DEBUG_MODE = <?php if(DEBUG_MODE) {echo 'true'; } else { echo 'false'; } ?>;
	
	// initializes the IDCheck variables used to authenticate this user's AJAX communications with the server
	window.IDCheck1 = "<?php echo $_SESSION['token']->getSessionID(); ?>";
	window.IDCheck2 = "<?php echo $_GET['fingerprint']; ?>";

	$(document).ready(function()
	{
		// initializes the dialog windows on the page
		$("#databaseSetupPopup").dialog({ autoOpen: false, modal: true, width: 900, show: "fade", hide: "fade", bgiframe: false});
		$("#databaseSetupResultPopup").dialog({ autoOpen: false, modal: true, width: 900, show: "fade", hide: "fade", bgiframe: false});
		$("#categoryCreationPopup").dialog({ autoOpen: false, modal: true, width: 1100, show: "fade", hide: "fade", bgiframe: false });
		$("#itemCreationPopup").dialog({ autoOpen: false, modal: true, width: 750, show: "fade", hide: "fade", bgiframe: false});
		$("#programOptionsPopup").dialog({ autoOpen: false, modal: true, width: 900, show: "fade", hide: "fade", bgiframe: false});
		$("#IPNLogPopup").dialog({ autoOpen: false, modal: true, width: 750, show: "fade", hide: "fade", bgiframe: false});
		$("#errorLogPopup").dialog({ autoOpen: false, modal: true, width: 750, show: "fade", hide: "fade", bgiframe: false});
		$("#counterPreviewPopup").dialog({ autoOpen: false, modal: true, width: "auto", height: "auto", show: "fade", hide: "fade", bgiframe: false});


		// stop if the database infrastructure needed by this system isn't present
		getIsDonationManagerSetUp(function(isDonationManagerSetUp) {
		
			if(!isDonationManagerSetUp)
			{
				alert("The database infrastructure needed by this system is not present. Check that the database schema set in the configuration " +
					"file is correct and that the database setup script located at \"database structure/current state.sql\" has been run in the correct schema.");
				return;
			}

			// checks whether the Donation Manager's options have been configured
			// if not, it guides the user through setting them
			getIsDonationTrackerConfigured(function(isDonationTrackerConfigured) {
			
				if(isDonationTrackerConfigured)
				{
					// disable the button that shows the IPN processor's errors if there are none
					getIPNProcessorErrorsExist(function(IPNProcessorErrorsExist) {
						if(!IPNProcessorErrorsExist)
						{
							$("#showErrorLogButton").attr("disabled", "disabled");
							$("#showErrorLogButton").addClass("ui-state-disabled");	// adds the disabled appearance to the jQuery UI button
						}
					});

					// disable the button that shows the IPNs if there are none
					getIPNsExist(function(IPNsExist) {
						if(!IPNsExist)
						{
							$("#showIPNLogButton").attr("disabled", "disabled");
							$("#showIPNLogButton").addClass("ui-state-disabled");	// adds the disabled appearance to the jQuery UI button
						}
					});

					refreshItemCategories();
				}
				else
				{
					// if the system isn't configured, guide the user through configuring its options
					showProgramOptionsPopup();
				}
			});
		});
	});

	</script>
</head>


<body>

<!-- urges users of IE 7 and below to upgrade their browser - code copied from The Internet Explorer 6 Countdown http://ie6countdown.com/ -->
<!--[if lte IE 7]> <div style='clear: both; height: 59px; text-align: center; margin-bottom: 0px; height: 42px;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->


<!-- this is the popup window that appears when a user wants to create a new item category -->
<div id="categoryCreationPopup" class="popupWindow" title="Create Item Category">
	<div class="newCategoryTypeSelection controls">
		<div><input type="radio" id="newCategorySimpleCountersOnly" name="newCategoryType" value="simple counters" onclick="categoryCreation_simpleCountersOptionSelected()" /><label for="newCategorySimpleCountersOnly">Simple counters: Just counts all money received for each item in this category</label></div>
		<div><input type="radio" id="newCategoryLimitedPages" name="newCategoryType" value="limited pages" onclick="categoryCreation_limitedPagesOptionSelected()" /><label for="newCategoryLimitedPages">Page-based counters: Allow a limited number of copies to be purchased for items in this category</label></div>
		<div><input type="radio" id="newCategoryInfinitePages" name="newCategoryType" value="unlimited pages" onclick="categoryCreation_unlimitedPagesOptionSelected()" /><label for="newCategoryInfinitePages">Page-based counters: Allow an unlimited number of copies to be purchased for items in this category</label></div>
	</div>
	<div>
		<div id="newCategoryNameContainer" class="inputElement">
			<p>Category Name</p>
			<div>
				<input type="text" />
			</div>
		</div>
		<div id="newCategoryPaymentThresholdContainer" class="inputElement">
			<p>Price per item</p>
			<div>
				<input type="text" />
			</div>
		</div>
		<div id="newCategoryMaxItemsAllowedContainer" class="inputElement">
			<p>Max copies per item</p>
			<div>
				<input type="text" />
			</div>
		</div>
	</div>
	<div>
		<button id="createCategorySubmit" class="ui-state-default ui-corner-all" type="button" onclick="createCategory()">Create Category</button>
		<button class="ui-state-default ui-corner-all" type="button" onclick="categoryCreationPopupVisibility(false)">Cancel</button>
	</div>
</div>


<!-- this is the popup window that appears when a user wants to create a new item -->
<div id="itemCreationPopup" class="popupWindow" title="Create Item">
	<div>
			<div id="newItemNameContainer" class="inputElement">
				<p>Name</p>
				<div>
					<input type="text" />
				</div>
			</div>
	</div>
	<div>
		<div id="newItemBlueprintContainer" class="wideInputElement">
			<p>Counter Blueprint</p>
			<select><option value="">No Counter Blueprints in the Database</option></select>
		</div>
	</div>
	<div>
		<button id="createItemSubmit" class="ui-state-default ui-corner-all" type="button" onclick="createItem()">Create Item</button>
		<button class="ui-state-default ui-corner-all" type="button" onclick="itemCreationPopupVisibility(false)">Cancel</button>
	</div>
</div>


<!-- this is the popup window that appears when a user wants to edit Donation Manager options -->
<div id="programOptionsPopup" class="popupWindow" title="Donation Manager Options">
	<div class="controls">
		<p class="title">Payment Calculation Mode</p>
		<div><input type="radio" id="programOptions_paymentCalculationNetOption" name="paymentCalculationMode" value="net" /><label for="programOptions_paymentCalculationNetOption">Net<div>The credited amount for each transaction is the amount of money that remains after taxes and Paypal fees are deducted.</div></label></div>
		<div><input type="radio" id="programOptions_paymentCalculationGrossOption" name="paymentCalculationMode" value="gross" /><label for="programOptions_paymentCalculationGrossOption">Gross<div>The credited amount for each transaction is the original amount given by the donor.</div></label></div>
	</div>
	<div class="controls">
		<p class="title">Referral ID Checking</p>
		<div><input type="radio" id="programOptions_skipReferralIDOption" name="referralID" value="false" onclick="programOptions_skipReferralIDOptionSelected()" /><label for="programOptions_skipReferralIDOption">Disable referral ID checking.<div>All IPNs will be processed regardless of which referral ID they are addressed to.</div></label></div>
		<div><input type="radio" id="programOptions_inputReferralIDOption" name="referralID" value="true" onclick="programOptions_inputReferralIDOptionSelected()" /><label for="programOptions_inputReferralIDOption">Only process IPNs addressed to my referral ID:</label></div>
		<div><input type="text" id="programOptions_referralIDInput" class="indent" maxlength="13" /></div>
	</div>
	<div>
		<button id="programOptions_applyChangesButton" class="ui-state-default ui-corner-all" type="button" onclick="changeProgramOptions()">Done</button>
		<button class="ui-state-default ui-corner-all" type="button" onclick="programOptionsPopupVisibility(false)">Cancel</button>
	</div>
</div>


<!-- this is the popup window that displays logged IPNs in the database -->
<div id="IPNLogPopup" class="popupWindow" title="IPN Log">
	<div class="controls">
		<img src="web_interface_support/images/2leftarrow_32.png" alt="" onclick="IPNLog_backupToBeginning()" />
		<img src="web_interface_support/images/1leftarrow_32.png" alt="" onclick="IPNLog_backup()" />
		<input id="IPNLog_currentIPNNum" type="text" onkeypress="if(isDigitOrEditingKey(event, this.value, false, false)) { IPNLog_advanceToSpecificIPN(event); return true; } else { return false; }" /> / <span id="IPNLog_numIPNs"></span>
		<img src="web_interface_support/images/1rightarrow_32.png" alt="" onclick="IPNLog_advance()" />
		<img src="web_interface_support/images/2rightarrow_32.png" alt="" onclick="IPNLog_advanceToEnd()" />
	</div>
	<div id="IPNLog_IPNDisplayList">
	
		<!-- the IPNDisplay div shown here will be overwritten by Javascript when the popup is populated with IPN details; it exists here as a prototype for
			 the structure of IPNDisplay divs -->
		<!-- id="IPNLog_IPN_1" -->
		<div class="IPNDisplay">
			<span class="IPNDBIndex">x</span>
			<p>Item: <span class="itemName">x</span></p>
			<p>Amount: <span class="amount">$x.xx</span></p>
			<p>Transaction Type: <span class="transactionType">x</span></p>
			<p>Time Received: <span class="timestamp">x</span></p>
			<p>IPN Text:<p class="IPNText"></p></p>
		</div>
		<div>
			<button class="ui-state-default ui-corner-all" type="button" onclick="IPNLogPopupVisibility(false)">Done</button>
		</div>
	</div>
</div>


<!-- this is the popup window that displays logged errors in the database -->
<div id="errorLogPopup" class="popupWindow" title="IPN Processor Error Log">
	<div class="controls">
		<img src="web_interface_support/images/2leftarrow_32.png" alt="" onclick="errorLog_backupToBeginning()" />
		<img src="web_interface_support/images/1leftarrow_32.png" alt="" onclick="errorLog_backup()" />
		<input id="errorLog_currentErrorNum" type="text" onkeypress="if(isDigitOrEditingKey(event, this.value, false, false)) { errorLog_advanceToSpecificError(event); return true; } else { return false; }" /> / <span id="errorLog_numErrors"></span>
		<img src="web_interface_support/images/1rightarrow_32.png" alt="" onclick="errorLog_advance()" />
		<img src="web_interface_support/images/2rightarrow_32.png" alt="" onclick="errorLog_advanceToEnd()" />
	</div>
	<div>
		<button class="ui-state-default ui-corner-all" type="button" onclick="errorLog_deleteDisplayedError()"><img src="web_interface_support/images/stop_22.png" alt=""> Erase This Error</button>
	</div>
	<div id="errorLog_errorDisplayList">
	
		<!-- the errorDisplay div shown here will be overwritten by Javascript when the popup is populated with error details; it exists here as a prototype for
			 the structure of errorDisplay divs -->
		<!-- id="errorLog_error_1" -->
		<div class="errorDisplay">
			<span class="errorDBIndex">x</span>
			<span class="errorDisplayIndex">x</span>
			<p>Error Message: <span class="errorMessage"></span></p>
			<p>Time Received: <span class="timestamp">x</span></p>
			<p>IPN Text:<p class="errorIPNText"></p></p>
		</div>
		<div>
			<button class="ui-state-default ui-corner-all" type="button" onclick="errorLogPopupVisibility(false)">Done</button>
		</div>
	</div>
</div>


<!-- this is the popup window that displays logged errors in the database -->
<div id="counterPreviewPopup" class="popupWindow" title="Donation Counter for [item name filled in by Javascript]">
	<div>
		<p class="informationHeading">URL</p>
		<div id="counterPreviewPopup_counterURL" class="informationDisplay"><span>The URL will be filled in here by the Javascript that makes this popup visible.</span></div>
	</div>
	<div>
		<p class="informationHeading">Embeddable Image</p>
		<div id="counterPreviewPopup_counterImageHTML" class="informationDisplay"><span>The image HTML will be filled in here by the Javascript that makes this popup visible.</span></div>
	</div>
	<div>
		<img id="counterPreviewPopup_counterImage" src="" alt="The donation counter failed to load." />
	</div>
</div>




<!-- this is the content that appears by default on the page -->
<div id="pageContent">
	<h4 class="pageHeading">Donation Manager</h4>

	<!-- the controls for managing items and item categories are contained in this div -->
	<div id="itemControls">
	
		<!-- the list of item categories will be inserted into the empty div contained in this div by Javascript on page load -->
		<div id="itemCategoryDisplay">
			<h5 class="title">Item Categories</h5>
			<div>
			</div>
			<button class="ui-state-default ui-corner-all" type="button" onclick="categoryCreationPopupVisibility(true)"><img src="web_interface_support/images/folder.png" alt="" />Create New Category</button>
			<button class="ui-state-default ui-corner-all" type="button" onclick="deleteItemCategory()"><img src="web_interface_support/images/sc26050.png" alt="" />Remove Category</button>
		</div>
		
		<!-- the list of items within the selected category will be inserted into this div's table by Javascript when an item
		category is clicked -->
		<div id="itemsDisplay">
			<div id="itemsTableCoverup">
				<h4><img src="web_interface_support/images/previous.png" alt="left arrow" /> Choose a category from the left to get started.</h4>
			</div>
			<h5 class="title">List of Items</h5>

			<!-- this div exists because browsers are utterly incapable of having a scroll bar on a tbody element while the table rows are allowed
			to fluidly resize to fit their contents -->
			<div class="tableScrollHack">
			<table>
				<thead>
					<tr>
						<th class="smallColumn">Enabled</th>
						<th>Name</th>
						<th class="smallColumn">Donations</th>
						<th class="smallColumn">Items Owed</th>
						<th class="smallColumn">Items Given</th>
						<th class="smallColumn">Price/Page</th>
						<th>Donation Counter</th>
					</tr>
				</thead>
				<!-- the list of items will be inserted here by Javascript when a category is selected -->
			</table>
			</div>
			<div class="showOnlyEnabledItemsContainer">
				<input id="showOnlyEnabledItemsControl" type="checkbox" checked="checked" onclick="refreshItems()" /><label for="showOnlyEnabledItemsControl">Show only Enabled items</label>
			</div>
		</div>
	</div>
	
	<div id="programOptions">
		<h5 class="title">Miscellaneous Controls</h5>
		<div>
			<button id="showProgramOptionsButton" class="ui-state-default ui-corner-all" type="button" onclick="showProgramOptionsPopup()">Edit Options</button>
			<button id="showIPNLogButton" class="ui-state-default ui-corner-all" type="button" onclick="showIPNLogPopup()">View IPNs</button>
			<button id="showErrorLogButton" class="ui-state-default ui-corner-all" type="button" onclick="showErrorLogPopup()">View Error Log</button>
		</div>
	</div>
</div>
<?php include('web_interface_support/footer.php'); ?>
</body>
</html>
