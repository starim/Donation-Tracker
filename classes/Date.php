<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This is a class for storing dates and times that makes it easy to store a single date and time and retrieve it in a human readable format (US format), PHP format
 * (Unix timestamp), or MySQL format.
 * This class is a wrapper for the PHP DateTime class.
 * 
 */

class Date
{
	// the date and time are internally stored as a PHP DateTime object
	private $timestamp;
	
	
	// constructor
	// $datetime is a string representing the date and time of this DateTime object in standard US format ("MM/DD/YYYY HH:MM:SS"), or any format recognized by
	// PHP's strtotime() function
	// if $dateTime is an empty string, this object will use the current time at the moment this constructor was called
	//
	// if $dateTime cannot be parsed into a valid date and time, an exception is thrown
	public function Date($dateTime)
	{
		try
		{
			$this->timestamp = new DateTime($dateTime);
		}
		catch(Exception $exception)
		{			
			throw new Exception("Error creating date/time: $dateTime is not in a recognized format.\n".$exception->getMessage());
		}
	}
	
	// returns a PHP DateTime object representing this date
	public function getDateTime()
	{
		return $this->timestamp;
	}
	
	// returns a Unix timestamp representing this date
	public function getUnixTimestamp()
	{
		return $this->timestamp->format('U');
	}
	
	// returns a string representing this date in the standard MySQL datetime format (YYYY-MM-DD HH:MM:SS)
	public function getMySQLDateTime()
	{
		return $this->timestamp->format('Y-m-d H:i:s');
	}
	
	// returns a string representing this date in the standard MySQL date format (YYYY-MM-DD)
	public function getMySQLDate()
	{
		return $this->timestamp->format('Y-m-d');
	}
	
	// returns a string representing this date in the standard US format (MM/DD/YYYY HH:MM:SS)
	// if twelveHourClock is set to true, a 12 hour time with AM/PM appended is used, otherwise the 24 hour clock is used
	// if verbose is set to true, the name of the day is prepended to the date, the month's name is given instead of its number, and the day is listed with its
	// proper suffix (for example, "Fri Apr 5th, 1985 3:34:00 PM" if twelveHourClock was true)
	// the seconds parameter is a boolean that sets whether or not seconds are included
	public function getStandardUSDateTime($twelveHourClock=false, $verbose = false, $seconds=true)
	{
		if($verbose)
		{
			return $this->timestamp->format('D M jS, Y '.($twelveHourClock ? 'g:i'.($seconds ? ':s' : '').'A' : 'H:i'.($seconds ? ':s' : '')));
		}
		else
		{
			return $this->timestamp->format('n/j/Y '.($twelveHourClock ? 'g:i'.($seconds ? ':s' : '').'A' : 'h:i'.($seconds ? ':s' : '')));
		}
	}
	
	// returns a string representing this date in the standard US format (MM/DD/YYYY)
	// if verbose is set to true, the name of the day is prepended to the date, the month's name is given instead of its number, and the day is listed with its
	// proper suffix (for example, "Fri Apr 5th, 1985")
	public function getStandardUSDate($verbose = false)
	{
		if($verbose)
		{
			return $this->timestamp->format('D M jS, Y');
		}
		else
		{
			return $this->timestamp->format('n/j/Y');
		}
	}
}

?>