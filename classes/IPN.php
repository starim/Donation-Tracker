<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script contains the class that represents an Instant Payment Notification (IPN).
 * 
 */


require_once('configuration.php');
require_once('DBConnection.php');
require_once('exceptions/IPNProcessorException.php');

// this class represents an Instant Payment Notification (IPN) sent from Paypal
class IPN
{	
	// an object of class DBConnection storing a link to the database
	private $database;
	
	// information sent by Paypal as part of the IPN
	private $paymentStatus;
	private $amount;
	private $currency;
	private $itemID;
	private $transactionID;
	private $receiverID;
	private $payerEmail;
	private $transactionType;
	
	// the full text of the IPN sent by Paypal
	// this is used when logging IPNs
	private $originalIPNText;
	
	
	// constructor
	public function IPN($IPNText, $paymentStatus, $paymentGrossAmount, $paymentFee, $transactionID, $transactionType, $receiverID, $payerEmail, $itemName)
	{
		$this->setOriginalText($IPNText);
		$this->setPaymentStatus($paymentStatus);

		// Paypal does not implement automatic currency conversion so if a payment is received in a currency other than the seller's native currency the amount
		// in paymentAmount will have to be adjusted
		// currently the system does not do this so all payment must occur in the seller's native currency
		$this->setCurrency(null);	// $this->setCurrency($paymentCurrency);
		$this->setTransactionID($transactionID);
		$this->setReceiverID($receiverID);
		$this->setPayerEmail($payerEmail);
		$this->setTransactionType($transactionType);
		
		// gets the ID number of the item this payment applies to
		$database = new DBConnection(false);
		$this->setDatabase($database);
		$cleanItemName = $database->sanitize($itemName);
		$result = $this->database->query("SELECT ID FROM ".DBConnection::$itemsTable." WHERE Name='$cleanItemName'");
		if(mysql_num_rows($result) == 0)
			throw new IPNProcessorException("Unrecognized item: \"$itemName\". No item with this name could be found in the database.", $IPNText);
		
		$this->setItemID(mysql_result($result, 0, 0));
		
		// check which payment calculation mode is active and use that mode to determine the payment amount
		$paymentCalculationMode = $database->getOptionValue('PAYMENT_CALCULATION_MODE');
		if(strcasecmp($paymentCalculationMode, 'gross') == 0)
			$this->setPaymentAmount($paymentGrossAmount);
		else
			$this->setPaymentAmount($paymentGrossAmount - $paymentFee);
	}
	
	// returns whether this IPN represents a completed payment transaction
	private function isTransactionComplete()
	{
		return (strcasecmp($this->getPaymentStatus(), 'completed') == 0);
	}
	
	// returns whether this IPN represents a transaction type that this sytem can process
	private function isTransactionTypeAccepted()
	{
		$transactionType = $this->getTransactionType();
		return (($transactionType == 'cart') || ($transactionType == 'express_checkout') || ($transactionType == 'masspay')
				|| ($transactionType == 'recurring_payment') /* || ($transactionType == 'send_money') */ || ($transactionType == 'subscr_payment')
				|| ($transactionType == 'virtual_terminal') || ($transactionType == 'web_accept') || ($transactionType == 'adjustment'));
	}
	
	// returns true if the transaction ID of this IPN is already recorded in the database, indicating it has already been handled
	private function isDuplicate()
	{
		try
		{
			$database = $this->getDatabase();
			$cleanTransactionID = $database->sanitize($this->getTransactionID());
			$result = $database->query("SELECT * FROM ".DBConnection::$IPNRecordTable." WHERE transactionID='$cleanTransactionID'");
			
			return (mysql_num_rows($result) > 0);
		}
		catch(Exception $exception)
		{
			throw new IPNProcessorException($exception->getMessage(), $this->getOriginalIPNText());
		}
	}
	
	// checks whether this payment was meant for this recipient by checking the IPN's receiver ID against the seller's receiver ID (also known as a referral ID)
	// referral ID checking is turned off if MY_REFERRAL_ID is set to an empty string; in this case the function will always return true
	private function isForMe()
	{
		$database = $this->getDatabase();
		$referralID = $database->getOptionValue('MY_REFERRAL_ID');
			
		return ($referralID == '' || ($referralID == $this->getReceiverID()));
	}
	
	// returns whether the item this payment is intended for is currently active and not discontinued
	private function isItemActive()
	{
		$database = $this->getDatabase();
		$cleanItemID = $database->sanitize($this->getItemID());
		$result = $database->query("SELECT Active FROM ".DBConnection::$itemsTable." WHERE ID='$cleanItemID'");
		return mysql_result($result, 0, 0);
	}
	
	// logs this IPN to the database (this should be done *only* after the IPN has been verified by Paypal!)
	public function logIPN()
	{
		$database = $this->getDatabase();
		
		// run this IPN through a series of checks before processing it
		
		// checks that this transaction has not been previously processed
		if($this->isDuplicate())
			throw new IPNProcessorException('This IPN is a duplicate of an already processed IPN (it has a transaction ID of '.$this->getTransactionID().', which is already associated with an IPN in the IPN Records table).', $this->getOriginalText());
		
		// checks that this IPN represents a completed payment transaction; if not, ignore it
		if(!$this->isTransactionComplete())
			throw new IPNProcessorException('This IPN represents an incomplete transaction (it has a payment status of "'.$this->getPaymentStatus().'").', $this->getOriginalText());
			
		if(!$this->isTransactionTypeAccepted())
			throw new IPNProcessorException('This IPN has a transaction type ('.$this->getTransactionType().') that this system does not currently process.', $this->getOriginalText());
			
		if(!$this->isItemActive())
			throw new IPNProcessorException('This IPN represents a payment toward a discontinued item.', $this->getOriginalText());
		
		// checks that the IPN was intended for this seller
		if(!$this->isForMe())
			throw new IPNProcessorException('Referral ID mismatch between the ID for this seller and the referral ID of this IPN\'s intended recipient.', $this->getOriginalText());
			
		
		// begin the actual IPN logging
		
		// sanitize variables
		$cleanTransactionID = $database->sanitize($this->getTransactionID());
		$cleanAmount = $database->sanitize($this->getPaymentAmount());
		$cleanItemID = $database->sanitize($this->getItemID());
		$cleanPayerEmail = $database->sanitize($this->getPayerEmail());
		$cleanIPNText = $database->sanitize($this->getOriginalText());
		$cleanTransactionType = $database->sanitize($this->getTransactionType());
		
		try
		{
			$database->beginTransaction();
			
			// log the IPN
			$database->query("INSERT INTO ".DBConnection::$IPNRecordTable." 
								SET TransactionID='$cleanTransactionID', 
								Amount='$cleanAmount', 
								ItemID='$cleanItemID', 
								PayerEmail='$cleanPayerEmail', 
								OriginalText='$cleanIPNText', 
								TransactionType='$cleanTransactionType', 
								Timestamp=NOW()");
			
			// get data on this item
			$result = $database->query("SELECT IF(I.PriceOverride=0.00, IC.PaymentThreshold, I.PriceOverride) AS 'PaymentThreshold', IC.MaxItemsAllowed, I.Name, I.ItemsOwed, I.ItemsGiven, I.Donations
										FROM ".DBConnection::$itemCategoriesTable." IC
											LEFT JOIN ".DBConnection::$itemsTable." I ON I.CategoryID=IC.ID
										WHERE I.ID='$cleanItemID' LIMIT 1");
			$itemData = mysql_fetch_assoc($result);
											
			// calculate the current donation amount for this item, rounding to two decimal places
			$currentAmount = $itemData['Donations'] + number_format($this->getPaymentAmount(), 2, '.', '');
			
			// calculate the new number of items owed and the new donation amount
			// if the payment threshold is 0 then this item functions simply as a counter--it can never have items owed
			if($itemData['PaymentThreshold'] != 0)
			{
				$newItemsOwed = floor($currentAmount/$itemData['PaymentThreshold']);
				$newAmount = fmod($currentAmount, $itemData['PaymentThreshold']);
			}
			else
			{
				$newItemsOwed = 0;
				$newAmount = $currentAmount;
			}
			
			// check whether this donation has put the item over its allowed maximum number of purchases
			if(($itemData['MaxItemsAllowed'] != 0) && (($itemData['ItemsOwed'] + $itemData['ItemsGiven'] + $newItemsOwed + $newAmount/$itemData['PaymentThreshold']) > $itemData['MaxItemsAllowed']))
			{
				// store the amount by which this donation exceeds the maximum allowed and log this in the errors table
				// this excess money will have to handled by the seller
				$excessAmount = ($itemData['ItemsOwed'] + $itemData['ItemsGiven'] + $newItemsOwed - $itemData['MaxItemsAllowed']) * $itemData['PaymentThreshold'] + $newAmount;

				$cleanErrorMessage = $database->sanitize("A payment was received that exceeded the maximum amount that could be donated to item {$itemData['Name']} by $".number_format($excessAmount, 2)
									.(($this->getPayerEmail() != '') ? '. The donor\'s email address is '.$this->getPayerEmail() : '. The IPN did not list an email address for this donor.'));
				$cleanIPNText = $database->sanitize($this->getOriginalText());
				$database->query("INSERT INTO `".DBConnection::$donationTrackerSchema."`.Errors SET ErrorMessage='$cleanErrorMessage', IPNText='$cleanIPNText'");
				
				$newItemsOwed = $itemData['MaxItemsAllowed'] - $itemData['ItemsOwed'] - $itemData['ItemsGiven'];
				$newAmount = 0.0;
			}
			
			// update the item's data in the database
			if($newItemsOwed != 0)
			{
				// if the number of items owed changed, update both the Donations and ItemsOwed columns
				$cleanNewItemsOwed = $database->sanitize($newItemsOwed);
				$cleanNewAmount = $database->sanitize($newAmount);
				$database->query("UPDATE ".DBConnection::$itemsTable." SET Donations='$cleanNewAmount', ItemsOwed=ItemsOwed+'$cleanNewItemsOwed' WHERE ID='$cleanItemID'");
			}
			else
			{
				// if the ItemsOwed counter didn't change, just update the Donations column
				$cleanNewAmount = $database->sanitize($newAmount);
				$database->query("UPDATE ".DBConnection::$itemsTable." SET Donations='$cleanNewAmount' WHERE ID='$cleanItemID'");
			}
			

			$database->commitTransaction();
											
			// check the IPN_Records table for old records that can be cleaned up
			$database->cleanupIPNTable();
		}
		catch(Exception $exception)
		{
			$this->getDatabase()->rollbackTransaction();
			throw new IPNProcessorException($exception->getMessage(), $this->getOriginalText());
		}
	}
	
	
	// standard set of getters and setters for class properties
	
	private function getPaymentStatus()
	{
		return $this->paymentStatus;
	}
	private function getPaymentAmount()
	{
		return $this->amount;
	}
	private function getCurrency()
	{
		return $this->currency;
	}
	private function getReceiverID()
	{
		return $this->receiverID;
	}
	private function getPayerEmail()
	{
		return $this->payerEmail;
	}
	private function getItemID()
	{
		return $this->itemID;
	}
	private function getTransactionID()
	{
		return $this->transactionID;
	}
	private function getDatabase()
	{
		return $this->database;
	}
	private function getOriginalText()
	{
		return $this->originalIPNText;
	}
	private function getTransactionType()
	{
		return $this->transactionType;
	}
	
	
	private function setPaymentStatus($newStatus)
	{
		$this->paymentStatus = $newStatus;
	}
	private function setPaymentAmount($newAmount)
	{
		$this->amount = $newAmount;
	}
	private function setCurrency($newCurrency)
	{
		$this->currency = $newCurrency;
	}
	private function setReceiverID($newID)
	{
		$this->receiverID = $newID;
	}
	private function setPayerEmail($newEmailAddress)
	{
		$this->payerEmail = $newEmailAddress;
	}
	private function setItemID($newItemID)
	{
		$this->itemID = $newItemID;
	}
	private function setTransactionID($newTransactionID)
	{
		$this->transactionID = $newTransactionID;
	}
	private function setDatabase($newDBLink)
	{
		$this->database = $newDBLink;
	}
	private function setOriginalText($originalText)
	{
		$this->originalIPNText = $originalText;
	}
	private function setTransactionType($newType)
	{
		$this->transactionType = $newType;
	}
}

?>
