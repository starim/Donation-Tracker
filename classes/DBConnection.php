<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script contains the class that manages a MySQL database connection.
 * 
 */
 
require_once(dirname(__FILE__).'/../configuration.php');
require_once('session.php');
require_once('exceptions/DBException.php');


// this class allows creation and manipulation of a MySQL database connection
class DBConnection
{
	// stores the link for this database connection
	private $databaseLink;
	
	// stores a sanitized copy of the name of the schema the donation tracker uses	
	public static $donationTrackerSchema;
	
	// these store the paths for the database tables that exist in the IPN processing system
	// these are fully qualified and sanitized so they can be used directly in SQL queries without any danger
	public static $itemCategoriesTable;
	public static $itemsTable;
	public static $errorsTable;
	public static $IPNRecordTable;
	public static $optionsTable;
	public static $counterBlueprintsTable;
	public static $counterTextElementsTable;
	public static $counterProgressBarsTable;
	
	// stores an array of unsanitized table names corresponding to the tables above
	// this is used when checking whether all required tables are present when initializing DBConnection objects
	private static $requiredTablesList;
	
	// the set of default options, which will be used if these values aren't set in the Options table in the database
	private static $DEFAULT_OPTIONS = array('MY_REFERRAL_ID' => '', 'PAYMENT_CALCULATION_MODE' => 'net');
	
	
	// constructor
	// donationManagerSession sets whether to require a proper PHP session and fingerprint before logging into the database; set this to true for anything relating
	// to the donation manager user interface, and set it to false for the IPN processor which will not have a PHP session
	public function __construct($donationManagerSession = true)
	{
		if($donationManagerSession)
		{
			if(!session::isLoggedIn())
				throw new Exception('User authentication failed. You must be logged in to the donation tracker system in order to execute this script.');

			$this->databaseLink = $_SESSION['token']->databaseLogin(DBConnection::getDatabaseServer());
		}
		else
		{
			// use connection parameters set in the configuration file, or if they are blank use the defaults in php.ini 
			$user = (DB_USER == '' ?  ini_get('mysql.default_user') : DB_USER);
			$password = (DB_PASSWORD == '' ? ini_get('mysql.default_password') : DB_PASSWORD);

			$this->databaseLink = DBConnection::connect(DBConnection::getDatabaseServer(), $user, $password);
		}
			
		// select the schema that will be used
		mysql_select_db(DB_SCHEMA);
			
		// set the database table paths if they haven't already been set
		if(is_null(DBConnection::$donationTrackerSchema))
		{
			DBConnection::$donationTrackerSchema = mysql_escape_string(DB_SCHEMA);
			
			DBConnection::$itemCategoriesTable = '`'.DBConnection::$donationTrackerSchema.'`.`Item_Categories`';
			DBConnection::$itemsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Items`';
			DBConnection::$errorsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Errors`';
			DBConnection::$IPNRecordTable = '`'.DBConnection::$donationTrackerSchema.'`.`IPN_Record`';
			DBConnection::$optionsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Options`';
			DBConnection::$counterBlueprintsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Donation_Counters_Blueprints`';
			DBConnection::$counterTextElementsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Donation_Counters_Text_Elements`';
			DBConnection::$counterProgressBarsTable = '`'.DBConnection::$donationTrackerSchema.'`.`Donation_Counters_Progress_Bars`';
			
			DBConnection::$requiredTablesList = array('Item_Categories', 'Items', 'Errors', 'IPN_Record', 'Options', 'Donation_Counters_Blueprints',
					'Donation_Counters_Text_Elements', 'Donation_Counters_Progress_Bars');
		}
		
		// set the character encoding the server should expect from this client
		$charset = ini_get("mbstring.internal_encoding");
		if($charset !== false && $charset != '')
		{
				$cleanCharacterSetName = mysql_real_escape_string($charset);
				$this->query("SET NAMES '$cleanCharacterSetName';");
				
				// this is a simpler way to set the character set, but is only available in PHP 5.2.3 or later
				// mysql_set_charset($charset);
		}
	}
	
	// connect to the database at the IP address or domain name given by the server parameter, using the supplied credentials
	// returns the connection object if a connection could be established, otherwise throws an Exception
	public static function connect($server, $user, $password)
	{
		ob_start();
		$connection = mysql_connect($server, $user, $password);
		$connectErrorMessage = ob_get_clean();
		
		// throw an Exception here if the database connection could not be established
		if($connection === false)
		{
			$errorMessage = "Could not connect to MySQL server at $server with user name \"$user\".";
			if(DEBUG_MODE && ($connectErrorMessage != ''))
				$errorMessage .= " The specific error is: $connectErrorMessage.";
			
			throw new Exception($errorMessage);
		}
		
		return $connection;
	}
	
	// a string containing the database server's name and the port to connect on
	// this string is ready to be passed directly to mysql_connect
	public static function getDatabaseServer()
	{
		// use connection parameters set in the configuration file, or if they are blank use the defaults in php.ini
		return (DB_SERVER == '' ?  ini_get('mysql.default_host') : DB_SERVER.':'.DB_PORT);
	}
	
	// returns an array of tables that are required by the donation tracker but aren't present in the set database schema
	// if tables are missing, the system shouldn't be expected to function correctly and the setup script will need to be re-run
	// throws an Exception if checking the database fails with an SQL error
	public function donationTrackerDBCheck()
	{
		try
		{
			$result = $this->query("SHOW TABLES IN `".DBConnection::$donationTrackerSchema."`;");
		}
		catch(DBException $exception)
		{
			// if the database hasn't been set up yet, it's very possible that the schema simply doesn't exist, causing the above
			// query to fail with either a 1044-Access Denied or 1049-No Such Database error
			// in that case, mark all the required tables as missing instead of throwing an exception
			if($exception->isMySQLErrorDataAvailable() &&
				($exception->getMySQLErrorCode() == 1044 || $exception->getMySQLErrorCode() == 1049))
				return DBConnection::$requiredTablesList;
			else
				throw $exception;
		}
		
		$currentTablesList = array();
		for($i = 0; $i < mysql_num_rows($result); $i++)
			$currentTablesList[] = mysql_result($result, $i, 0);
		
		return array_diff(DBConnection::$requiredTablesList, $currentTablesList);
	}
	
	// returns a boolean indicating whether this Donation Manager's options are set
	public function isDonationTrackerDBConfigured()
	{
		$result = $this->query("SELECT COUNT(*) FROM ".DBConnection::$optionsTable.";");
		return (mysql_result($result, 0, 0) > 0);
	}
	
	// starts a transaction
	public function beginTransaction()
	{
		$this->query('START TRANSACTION', $this->getDatabaseLink());
	}
	
	// commits a transaction
	public function commitTransaction()
	{
		$this->query('COMMIT', $this->getDatabaseLink());
	}
	
	// rolls back a transaction
	public function rollbackTransaction()
	{
		$this->query('ROLLBACK', $this->getDatabaseLink());
	}
	
	// executes the MySQL query given by queryString
	// throws a DBException if the query fails (this behavior can be overridden by setting the $ignoreError flag to true)
	public function query($queryString, $ignoreError = false)
	{
		$result = mysql_query($queryString, $this->getDatabaseLink());
		if(!$ignoreError)
			$this->checkError($queryString);
		
		return $result;
	}
	
	// checks whether there was an error with the last MySQL operation and if so, throws a DBException
	// $queryString gives the text of the query that was executed so that it can included in the error message
	private function checkError($queryString)
	{
		$error = mysql_error($this->getDatabaseLink());
		
		if($error != '')
			throw new DBException('Error when querying the database', $this->getDatabaseLink(), $queryString);
	}
	
	// closes the MySQL connection associated with this DBConnection object
	// calling this function isn't necessary unless the script using the DBConnection object is expected to run frequently and finishes using
	// the database a long time before the script finishes; in this case saving resources is a priority so calling this function once database
	// querying is finished will help conserve memory and database connections
	public function closeConnection()
	{
		mysql_close($this->getDatabaseLink());
	}
	
	// returns a sanitized version of the passed variable that can be safely used in MySQL queries
	public function sanitize($variable)
	{
		return mysql_real_escape_string($variable, $this->getDatabaseLink());
	}
	
	// deletes rows in the IPN_Records table older than 5 days (records must be retained for 4 days to work correctly with Paypal's system)
	// since this is just a cleanup routine, an exception will not be thrown if a MySQL error is encountered
	public function cleanupIPNTable()
	{
		$this->query("DELETE FROM `".DBConnection::$donationTrackerSchema."`.`IPN_Record` WHERE Timestamp < DATE_SUB(NOW(), INTERVAL 5 DAY)", true);
	}
	
	private function getDatabaseLink()
	{
		return $this->databaseLink;
	}
	
	// a convenience function that wraps the getOptionsValues() function
	// this function takes a single option name and simply returns its set value, or null if it is not set
	public function getOptionValue($optionName)
	{
		$optionsMap = $this->getOptionsValues(array($optionName));
		return $optionsMap[$optionName];
	}
	
	// returns an associative array mapping option names to their values for all options whose names appear in the passed $optionNames array
	// simply returns the value instead of an associative array with one key-value pair if only one option value was requested
	// options that were requested but don't have a value set will be mapped to null
	public function getOptionsValues($optionNames)
	{
		$optionsMap = array();
		foreach($optionNames as $key)
		{
			$cleanOptionName = mysql_real_escape_string($key);
			$result = $this->query("SELECT `Value` FROM ".DBConnection::$optionsTable." WHERE `Key`='$cleanOptionName' LIMIT 1;");
			if(mysql_num_rows($result) == 0)
				$optionsMap[$key] = null;
			else
				$optionsMap[$key] = mysql_result($result, 0, 0);
		}

		// if somehow some of the default options aren't set, fill in their default values
		foreach(DBConnection::$DEFAULT_OPTIONS as $key => $defaultValue)
		{
			// skip over values that weren't requested
			if(!in_array($key, $optionNames))
				continue;
			
			if(is_null($optionsMap[$key]))
				$optionsMap[$key] = $defaultValue;
		}
		
		return $optionsMap;
	}
	
	// a convenience function that wraps the setOptionsValues() function
	// simply sets a single option whose name and value are passed in
	public function setOptionValue($optionName, $value)
	{
		$this->setOptionsValues(array($optionName => $value));
	}
	
	// sets the value for the configuration option with the specified name
	public function setOptionsValues($optionsMap)
	{
		foreach($optionsMap as $key => $value)
		{
			$cleanOptionName = mysql_real_escape_string($key);
			$cleanValue = mysql_real_escape_string($value);
			$result = $this->query("SELECT COUNT(*) FROM ".DBConnection::$optionsTable." WHERE `Key`='$cleanOptionName' LIMIT 1;");
			if(mysql_result($result, 0, 0) == 0)
				$this->query("INSERT INTO ".DBConnection::$optionsTable." (`Key`, `Value`) VALUES ('$cleanOptionName', '$cleanValue');");
			else
				$this->query("UPDATE ".DBConnection::$optionsTable." SET `Value`='$cleanValue' WHERE `Key`='$cleanOptionName';");
		}
	}
	
	// returns all currently set configurable options and their values in an associative array
	public function getAllOptionsValues()
	{
		$result = $this->query("SELECT * FROM ".DBConnection::$optionsTable.";");
		
		$optionsMap = array();
		while(($thisOption = mysql_fetch_assoc($result)) !== false)
			$optionsMap[$thisOption['Key']] = $thisOption['Value'];
		
		// if somehow some of the default options aren't set, fill in their default values
		foreach(DBConnection::$DEFAULT_OPTIONS as $key => $defaultValue)
		{
			if(!isset($optionsMap[$key]) || is_null($optionsMap[$key]))
				$optionsMap[$key] = $defaultValue;
		}
		
		return $optionsMap;
	}
}

?>
