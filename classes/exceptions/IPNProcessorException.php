<?php

/**
 * @author: Brent Houghton, © 2010
 * 
 * This script contains the class that represents an exception thrown during IPN processing. It differs from the standard Exception class by storing the text of
 * the IPN whose processing failed.
 * 
 */


class IPNProcessorException extends Exception
{
	private $IPNText;
	
	public function __construct($message, $IPNText)
	{
		$this->setIPNText($IPNText);
		
		parent::__construct($message);
	}
	
	private function setIPNText($newIPNText)
	{
		$this->IPNText = $newIPNText;
	}
	
	public function getIPNText()
	{
		return $this->IPNText;
	}
}

?>