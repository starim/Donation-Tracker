<?php

/**
 * @author: Brent Houghton, © 2010
 * 
 * This script contains the class that represents an exception thrown when a database operation fails. It differs from the standard Exception class by storing the
 * MySQL error numer and error message and optionally the text of the query that failed.
 * 
 */


class DBException extends Exception
{
	private $errorCode;
	private $errorMessage;
	private $queryString;
	
	// takes a message ($message) and optionally a database link identifier ($connection) and the text of the failed query ($connection)
	// if connection is specified the MySQL error number and error message will be retrieved and stored, otherwise only $message will be saved
	public function __construct($message = '', $connection = null, $queryString = null)
	{
		if(!is_null($connection))
		{
			$errorCode = mysql_errno($connection);
			$errorMessage = mysql_error($connection);
			$this->setMySQLErrorCode($errorCode);
			$this->setMySQLErrorMessage($errorMessage);
			$this->setQueryString($queryString);
			
			parent::__construct("$message (MySQL error $errorCode: $errorMessage).".(is_null($queryString) ? '' : " The exact query that failed was: \"$queryString\"."));
		}
		else
			parent::__construct($message);
	}
	
	// returns a boolean indicating whether or not the text of the failed query is stored in this exception
	public function isQueryStringAvailable()
	{
		return !is_null($this->queryString);
	}
	
	// returns a boolean indicating whether the MySQL error code and error message are stored in this exception
	public function isMySQLErrorDataAvailable()
	{
		return !is_null($this->errorCode);
	}
	
	
	// standard set of getters and setters
	
	private function setMySQLErrorCode($newErrorCode)
	{
		$this->errorCode = $newErrorCode;
	}
	
	public function getMySQLErrorCode()
	{
		return $this->errorCode;
	}
	
	private function setMySQLErrorMessage($newErrorMessage)
	{
		$this->errorMessage = $newErrorMessage;
	}
	
	public function getMySQLErrorMessage()
	{
		return $this->errorMessage;
	}
	
	private function setQueryString($newQueryString)
	{
		$this->queryString = $newQueryString;
	}
	
	public function getQueryString()
	{
		return (is_null($this->queryString) ? '' : $this->queryString);
	}
}

?>