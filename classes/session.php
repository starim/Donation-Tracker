<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script contains the class that manages all secure session-related processes.
 * 
 */

require_once('DBConnection.php');
require_once(dirname(__FILE__).'/../configuration.php');


class session
{
	// a copy of the current PHP session ID is kept here to be checked against session_id() to verify user identity
	private $sessionID = 'unset';
	
	// a unique value used for this session, separate from the PHP session ID, that provides a second method of checking user identity
	// unlike the session ID, this value is not refreshed to allow the user to use their browser's forward, back, and refresh buttons without
	// breaking the session
	private $fingerprint = 'unset';
	
	private $username = null;
	private $password = null;
	
	
	
	
	public function getSessionID()
	{
		return $this->sessionID;
	}
	
	// stores the current PHP session ID, or the specified ID
	private function setSessionID($newID = '')
	{
		if($newID == '')
		{
			$this->sessionID = session_id();
		}
		else
		{
			$this->sessionID = $newID;
		}
	}
	
	public function getFingerprint()
	{
		return $this->fingerprint;
	}
	
	// sets the fingerprint to a new random ID
	private function setFingerprint()
	{
		$this->fingerprint = sha1(uniqid(rand(), true) . $_SERVER['HTTP_USER_AGENT']);
	}
	
	private function getUsername()
	{
		return (is_null($this->username) ? '' : $this->username);
	}
	
	private function getPassword()
	{
		return (is_null($this->password) ? '' : $this->password);
	}
	
	// opens a database connection using the username and password that this user logged in with
	// returns the connection object if a connection could be established, otherwise throws an Exception
	public function databaseLogin($server)
	{
		return DBConnection::connect($server, $this->getUsername(), $this->getPassword());
	}
	
	// this function should be used when setting a new session ID
	// this should be done periodically to prevent session fixation attacks
	public function refreshSession()
	{
		session_regenerate_id(true);
		$this->setSessionID();
	}
	
	// a function for starting sessions that checks whether a session already exists before trying to create a new one
	public static function sessionStart()
	{
		if(session_id() == '')
		{
			session_start();
		}
	}
	
	// verifies that this session is legitimate by checking the current session ID and fingerprint parameter against their
	// internally stored counterparts
	// returns true if a session is deemed valid, false otherwise
	// session data is refreshed if the session is determined to be valid to prevent session fixation attacks
	private function verifySession()
	{
		/*
 		 * DEBUG - displays all contents of the SESSION and GET superglobals and the session ID and internally stored fingerprint in
 		 * 			nicely formatted XHTML, then dies
 		 *
		echo '<br />';
		echo 'Session ID: '.session_id();
		echo '<br /><br />';
		echo '$_SESSION: <br />';
		var_dump($_SESSION);
		echo '<br /><br />';
		echo '$_GET: <br />';
		var_dump($_GET);
		die();
		 */
		
		if(!isset($_GET['fingerprint']) || !isset($_SESSION['token']))
		{
			/*
			 * DEBUG - indicates why the session validation failed.
			 *
			 *
			 if(!isset($_GET['fingerprint']))
			 {
			 	echo("Error in session.php method verifySession(): no fingerprint set in GET.<br /><br />");
			 }
			 else
			 {
			 	echo("Error in session.php method verifySession(): no security token set in SESSION.");
			 }
			die();
			 */
			 
			return false;
		}

		if((session_id() == $this->getSessionID()) && ($_GET['fingerprint'] == $this->getFingerprint()))
			return true;
		else
		{
			/*
			 * DEBUG - indicates why the session validation failed.
			 *
			 *
			if(session_id() == $this->getSessionID())
			{
				echo("Error in session.php method verifySession(): sessionID mismatch (".session_id()." as PHPSESSID vs. $this->getSessionID() stored internally).");
			}
			echo "<br /><br />";
			if($_GET['fingerprint'] == $this->getFingerprint())
			{
				echo("Error in session.php method verifySession(): fingerprint mismatch (".$_GET['fingerprint']." in GET vs. $this->getFingerprint() stored internally).");
			}
			die();
			*/
			
			return false;
		}	
	}
	
	// this function prints a string meant to be appended to URLs used in anchor tags to prevent breaking the session when following links
	// if there is a session active, the fingerprint is printed in a form that can be added to the end of a URL; if there is no session nothing is printed
	public static function appendFingerprint()
	{
		if(isset($_SESSION['token']))
		{
			echo "?fingerprint=".$_SESSION['token']->getFingerprint();
		}
	}

	// this session tests whether a user has a valid session and is mostly a wrapper for the verifySession method
	public static function isLoggedIn()
	{	
		session::sessionStart();
		return(isset($_SESSION['token']) && $_SESSION['token']->verifySession());
	}
	
	
	
	
	// attemps to log a user in
	// session_start() must be called before calling this function--it is assumed a PHP session has already been created
	// returns true on successful login, false otherwise
	public function userLogin($userName, $password)
	{		
		// checks whether the user is already logged in and if so passes the user without creating a new session
		// this prevents users from creating multiple simultaneous sessions in order to carry out session fixation attacks
		if($this->verifySession())
			return true;
		
		if(!headers_sent())
			session_regenerate_id(true);
		
		// stores the username and password
		$this->username = $userName;
		$this->password = $password;

		// checks whether this username and password combination represent a valid login
		try
		{
			$this->databaseLogin(DBConnection::getDatabaseServer());
		}
		catch(Exception $exception)
		{
			if(DEBUG_MODE)
				throw $exception;
			else
				return false;
		}

		// stores the user's session ID and fingerprint if the login was successful
		$this->setSessionID();
		$this->setFingerprint();
		
		return true;
	}
	
	// destroys the user's session
	public function userLogoff()
	{
		session_destroy();
	}
	
	// a redirection function borrowed from Edoceo (http://edoceo.com/creo/php-redirect) and used with modifications
	// this function preserves secure sessions
	// $page is a string containing the path to the new page relative to the root directory, an absolute path, or a path relative to the calling script
	// $parameters is an optional parameter that consists of a string containing the parameters to be passed by GET
	// (do not include the ? but do include & between parameters)
	// for example, sessionRedirect("/folder/page.php", "var1=0&var2=0&var3=0");
	public function sessionRedirect($to, $parameters = '', $code=303)
	{
		session_write_close();
		
	  $location = null;
	  if (substr($to,0,4)=='http') $location = $to; // Absolute URL
	  else
	  {
	    $schema = $_SERVER['SERVER_PORT']=='443'?'https':'http';
	    $host = strlen($_SERVER['HTTP_HOST'] != 0)?$_SERVER['HTTP_HOST']:$_SERVER['SERVER_NAME'].($_SERVER['SERVER_PORT'] != 80 ? ':'.$_SERVER[SERVER_PORT] : '');
	    if (substr($to,0,1)=='/')
	    {
	    	$location = "$schema://$host$to";
	    }
	    else // Relative Path
	    {
	      $originatingURI = dirname($_SERVER['REQUEST_URI']);
	      if($originatingURI == '/')
	      	$originatingURI = '';
	      $location = "$schema://$host$originatingURI/$to";
	    }
	  }
	  
	  // add the fingerprint and any other GET parameters to the URL
	  $location .= '?fingerprint='.$this->getFingerprint().($parameters == '' ? '' : '&'.$parameters);
	
	  $hs = headers_sent();
	  if ($hs==false)
	  {
	  	switch($code)
	  	{
	  		case 301:
	  			header("HTTP/1.1 301 Moved Permanently"); // Convert to GET
	  			break;
	  			
	  		case 302:
	  			header("HTTP/1.1 302 Found"); // Conform re-POST
	  			break;
	  			
	  		case 303:
	  			header("HTTP/1.1 303 See Other"); // dont cache, always use GET
	  			break;
	  			
	  		case 304:
	  			header("HTTP/1.1 304 Not Modified"); // use cache
	  			break;
	  			
	  		case 305:
	  			header("HTTP/1.1 305 Use Proxy");
	  			break;
	  			
	  		case 306:
	  			header("HTTP/1.1 306 Not Used");
	  			break;
	  			
	  		case 307:
	  			header("HTTP/1.1 307 Temporary Redirect");
	  			break;
	  			
	  		default:
	  			trigger_error("Unhandled redirect() HTTP Code: $code",E_USER_ERROR);
	  	}

	    header("Location: $location");
	    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	  }
	  else
	  {
	    // todo: draw some javascript to redirect
	    $cover_div_style = 'background-color: #ccc; height: 100%; left: 0px; position: absolute; top: 0px; width: 100%;'; 
	    echo "<div style='$cover_div_style'>\n";
	    $link_div_style = 'background-color: #fff; border: 2px solid #f00; left: 0px; margin: 5px; padding: 3px; ';
	    $link_div_style.= 'position: absolute; text-align: center; top: 0px; width: 95%; z-index: 99;';
	    echo "<div style='$link_div_style'>\n";
	    echo "<p>Please See: <a href='$to'>".htmlspecialchars($location)."</a></p>\n";
	    echo "</div>\n</div>\n";
	  }
	  exit(0);
	}
	
	
	// a redirection function borrowed from Edoceo (http://edoceo.com/creo/php-redirect) and used with modifications
	// WARNING: this function will break secure session handling
	// use this to easily go a different page in situations where a secure session has not been established (i.e. if there is an error in a form and the
	// validation script needs to send the user page to the form's page to redo the form)
	// if a secure session has been established, use sessionRedirect() instead
	public static function redirect($to, $parameters = '', $code=303)
	{
	  $location = null;
	  if (substr($to,0,4)=='http') $location = $to; // Absolute URL
	  else
	  {
	    $schema = $_SERVER['SERVER_PORT']=='443'?'https':'http';
	    $host = (strlen($_SERVER['HTTP_HOST'] != 0)?$_SERVER['HTTP_HOST']:$_SERVER['SERVER_NAME']).($_SERVER['SERVER_PORT'] != 80 ? ':'.$_SERVER[SERVER_PORT] : '');
	    if (substr($to,0,1)=='/')
	    {
	    	$location = "$schema://$host$to";
	    }
	    else // Relative Path
	    {
	      $originatingURI = dirname($_SERVER['REQUEST_URI']);
	      if($originatingURI == '/')
	      	$originatingURI = '';
	      $location = "$schema://$host$originatingURI/$to";
	    }
	  }
	  
	  // add the GET parameters to the URL
	  $location .= ($parameters == '' ? '' : '?'.$parameters);

	
	  $hs = headers_sent();
	  if ($hs==false)
	  {
	  	switch($code)
	  	{
	  		case 301:
	  			header("HTTP/1.1 301 Moved Permanently"); // Convert to GET
	  			break;
	  			
	  		case 302:
	  			header("HTTP/1.1 302 Found"); // Conform re-POST
	  			break;
	  			
	  		case 303:
	  			header("HTTP/1.1 303 See Other"); // dont cache, always use GET
	  			break;
	  			
	  		case 304:
	  			header("HTTP/1.1 304 Not Modified"); // use cache
	  			break;
	  			
	  		case 305:
	  			header("HTTP/1.1 305 Use Proxy");
	  			break;
	  			
	  		case 306:
	  			header("HTTP/1.1 306 Not Used");
	  			break;
	  			
	  		case 307:
	  			header("HTTP/1.1 307 Temporary Redirect");
	  			break;
	  			
	  		default:
	  			trigger_error("Unhandled redirect() HTTP Code: $code",E_USER_ERROR);
	  	}

	    header("Location: $location");
	    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	  }
	  else
	  {
	    // todo: draw some javascript to redirect
	    $cover_div_style = 'background-color: #ccc; height: 100%; left: 0px; position: absolute; top: 0px; width: 100%;'; 
	    echo "<div style='$cover_div_style'>\n";
	    $link_div_style = 'background-color: #fff; border: 2px solid #f00; left: 0px; margin: 5px; padding: 3px; ';
	    $link_div_style.= 'position: absolute; text-align: center; top: 0px; width: 95%; z-index: 99;';
	    echo "<div style='$link_div_style'>\n";
	    echo "<p>Please See: <a href='$to'>".htmlspecialchars($location)."</a></p>\n";
	    echo "</div>\n</div>\n";
	  }
	  exit(0);
	}
		
}

?>
