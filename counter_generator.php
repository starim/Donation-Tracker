<?php

/**
 * @author: Brent Houghton, © 2010
 * 
 * A script for generating counters for items, defined by the blueprints in the Donation_Counters_Blueprints table in the database. Counters are images
 * that are primarily used to display donation progress for items, but can display certain other stats as well. 
 * 
 */


require_once('configuration.php');
require_once('classes/DBConnection.php');


// returns a true color image created from the given base PNG image
// this function is a true color version of the standard GD function imagecreatefrompng() and overcomes that function's lack of support for true color and
// transparency
function imagecreatetruecolorfrompng($baseImagePath)
{
	$dimensions = getimagesize($baseImagePath);
	$baseImage = imagecreatefrompng($baseImagePath);
	$trueColorImage = imagecreatetruecolor($dimensions[0], $dimensions[1]);
	imagealphablending($trueColorImage, false);
	imagesavealpha($trueColorImage, true);
	imagecopyresampled($trueColorImage, $baseImage,0,0,0,0, $dimensions[0], $dimensions[1], $dimensions[0], $dimensions[1]);
	imagealphablending($trueColorImage, true);
	return $trueColorImage;
}

// a helper function that calculates the height of some some text rendered in the specified font, with the specified point size, and using the string given in $text
// the text is assumed to be printed horizontally
function fontHeight($font, $points, $text)
{
	$fontDimensions = imagettfbbox($points, 0, $font, $text);
	
	// subtracts the y position of the upper left corner of the text box from the y position of the lower left corner and returns the result
	return	($fontDimensions[1] - $fontDimensions[7]);
}

// a helper function that calculates the width of some some text rendered in the specified font, with the specified point size, and using the string given in $text
// the text is assumed to be printed horizontally
function fontWidth($font, $points, $text)
{
	$fontDimensions = imagettfbbox($points, 0, $font, $text);
	
	// subtracts the x position of the lower left corner of the text box from the x position of the lower right corner and returns the result
	return	($fontDimensions[2] - $fontDimensions[0]);
}

// returns an array of 8 bit integers extracted from the given 24 bit integer
// the returned array has elements 'red', 'green', and 'blue' and red is assumed to be the highest 8 bits, green the middle 8 bits, and blue the lowest 8 bits
function convertColorIntToRGB($colorInt)
{
	$colorBinary = base_convert($colorInt, 10, 2);
	
	$redBinary = substr($colorBinary, 0, 8);
	$greenBinary = substr($colorBinary, 8, 8);
	$blueBinary = substr($colorBinary, 16, 8);
	
	$redInt = base_convert($redBinary, 2, 10);
	$greenInt = base_convert($greenBinary, 2, 10);
	$blueInt = base_convert($blueBinary, 2, 10);
	
	return array('red' => $redInt, 'green' => $greenInt, 'blue' => $blueInt);
}

// adds the text element with the specified properties to the given image
function addTextElement($database, $itemData, $image, $category, $elementID)
{
	global $debugOutputRequested;
	
	$cleanElementID = $database->sanitize($elementID);
	$elementDataResultSet = $database->query("SELECT * FROM ".DBConnection::$counterTextElementsTable." WHERE `Index`='$cleanElementID' LIMIT 1");
	
	// if the referenced element cannot be found, silently skip over it
	if(mysql_num_rows($elementDataResultSet) == 0)
		return;
		
	// get the element's properties
	$elementData = mysql_fetch_assoc($elementDataResultSet);
	
	// give the full path to the font file
	$elementData['FontFileName'] = 'fonts/'.$elementData['FontFileName'];
	
	
	// creates a string with the text that will be printed on the image
	switch($category)
	{
		case 'NameTextElement':
			$text = $itemData['Name'];
			break;
			
		case 'DonationsTextElement':

			if($itemData['MaxReached'])
				$text = '$'.$itemData['Price'];
			else
				$text = '$'.$itemData['Donations'];

			break;
			
		case 'ItemsOwedTextElement':
			$text = $itemData['ItemsOwed'];
			break;
			
		case 'ItemsGivenTextElement':
			$text = $itemData['ItemsGiven'];
			break;
			
		case 'PriceTextElement':
			$text = ($itemData['Price'] == 0 ? 'N/A' : '$'.$itemData['Price']);
			break;
		
		default:
			// if this category isn't recognized, silently skip over it
			return;
	}
	
	
	// calculates the x-coordinate of the text element
	if($elementData['XOffsetFrom'] == 'left')
		$xPosition = $elementData['XOffset'];
	else
		$xPosition = imagesx($image) - $elementData['XOffset'];

	// gives the element the correct justification
	switch($elementData['Justification'])
	{
		case 'left':
			break;
			
		case 'center':
			$xPosition -= floor(fontWidth($elementData['FontFileName'], $elementData['FontSize'], $text)/2);
			break;
			
		case 'right':
			$xPosition -= fontWidth($elementData['FontFileName'], $elementData['FontSize'], $text);
	}
		

	// calculates the y-coordinate of the text element
	if($elementData['YOffsetFrom'] == 'top')
		$yPosition = $elementData['YOffset'];
	else
		$yPosition = imagesy($image) - $elementData['YOffset']; + fontHeight($elementData['FontFileName'], $elementData['FontSize'], $text);
		

	// generates the color to use
	if($itemData['MaxReached'] && !is_null($elementData['MaxColorRGB']))
		$colorRGB = convertColorIntToRGB($elementData['MaxColorRGB']);
	else
		$colorRGB = convertColorIntToRGB($elementData['ColorRGB']);
	$color = imagecolorallocate($image, $colorRGB['red'], $colorRGB['green'], $colorRGB['blue']);
	

	if($debugOutputRequested)
		echo("Generating text element:<br />Category: $category, Value: $text, Color ID: $color, R: {$colorRGB['red']}, G: {$colorRGB['green']}, B: {$colorRGB['blue']}, x-coordinate: $xPosition, y-coordinate: $yPosition<br /><br />");
	

	imagettftext($image, $elementData['FontSize'], 0, $xPosition, $yPosition, $color, $elementData['FontFileName'], $text);
}

// adds the progress bar with the specified properties to the given image
function addProgressBar($image, $category, $elementID)
{
	global $debugOutputRequested;
	/*
	 * implement
	 */
	 
	if($debugOutputRequested)
		echo("Generating progress bar element:<br />Category: $category, Value: $text, Color ID: $color, R: {$colorRGB['red']}, G: {$colorRGB['green']}, B: {$colorRGB['blue']}, x-coordinate: $xPosition, y-coordinate: $yPosition<br /><br />");
}

// since this script is expected to produce its output in the form of images, error messages must be written onto an image that is returned
// this function creates an error message using the errorMessage string and sends the image back to the caller
function generateErrorImage($errorMessage)
{
	$font = 'fonts/Verdana_Bold.ttf';
	$fontSize = 10;
	
	$height = fontHeight($font, $fontSize, $errorMessage);
	$width = fontWidth($font, $fontSize, $errorMessage);
	
	$image = imagecreate($width + 20, $height + 20);
	imagecolorallocate($image, 255, 255, 255);
	$textColor = imagecolorallocate($image, 0, 0, 0);
	imagettftext($image, $fontSize, 0, 10, 20, $textColor, $font, $errorMessage);
	
	return $image;
}




// main script logic begins here


// HTTP/1.1 header to prevent browser caching
header('Cache-Control: no-store, no-cache, must-revalidate');

// set debug mode
// this mode will send text describing the steps taken to create the counter rather than the actual counter
global $debugOutputRequested;
$debugOutputRequested = (DEBUG_MODE && isset($_GET['DEBUG']) && ($_GET['DEBUG'] == '1'));

// output buffering is used to prevent warnings and notices from corrupting the image sent to the caller
ob_start();

if($debugOutputRequested)
	echo('Beginning script execution...<br /><br />');

try
{
	try
	{
		// this script requires that an item's name be passed to it
		if(!isset($_GET['item']))
			throw new Exception('No item name specified.');
	
		if($debugOutputRequested)
			echo('Connecting to database... ');
		$database = new DBConnection(false);
		if($debugOutputRequested)
			echo('successful<br /><br />');
		
		// get the item's data
		$cleanItemName = $database->sanitize($_GET['item']);
		$itemDataResultSet = $database->query("SELECT I.*, (IC.MaxItemsAllowed!=0 AND (I.ItemsOwed+I.ItemsGiven)=IC.MaxItemsAllowed) AS 'MaxReached', IF(I.PriceOverride=0, IC.PaymentThreshold, I.PriceOverride) AS 'Price' FROM ".DBConnection::$itemsTable." I
												LEFT JOIN ".DBConnection::$itemCategoriesTable." IC ON IC.ID=I.CategoryID
												WHERE I.Name='$cleanItemName' LIMIT 1");
		if(mysql_num_rows($itemDataResultSet) == 0)
			throw new Exception("No item was found with the name \"{$_GET['item']}\".");
		$itemData = mysql_fetch_assoc($itemDataResultSet);
		
		// get the counter blueprint
		$cleanBlueprintIndex = $database->sanitize($itemData['CounterBlueprint']);
		$counterDataResultset = $database->query("SELECT * FROM ".DBConnection::$counterBlueprintsTable." WHERE `Index`='$cleanBlueprintIndex' LIMIT 1");
		if(mysql_num_rows($counterDataResultset) == 0)
			throw new Exception("No counter blueprint was found in the database for the item named \"{$_GET['item']}\".");
		$counterData = mysql_fetch_assoc($counterDataResultset);
		
		
		// create the image
		$image = imagecreatetruecolorfrompng('images/'.$counterData['BaseImageFileName']);
		
		// reduce the blueprints array to just an array of references to elements
		unset($counterData['Index']);
		unset($counterData['ItemID']);
		unset($counterData['BaseImageFileName']);
		
		// cycles through each element and adds it to the image
		foreach($counterData as $elementCategory => $elementID)
		{
			// if there is no element for this category, skip to the next category
			if(is_null($elementID))
				continue;
				
			switch($elementCategory)
			{
				case 'NameTextElement':
				case 'DonationsTextElement':
				case 'ItemsOwedTextElement':
				case 'ItemsGivenTextElement':
				case 'PriceTextElement':
					addTextElement($database, $itemData, $image, $elementCategory, $elementID);
					break;
					
				case 'DonationsProgressBar':
				case 'ItemsOwedProgressBar':
				case 'ItemsGivenProgressBar':
					addProgressBar($database, $itemData, $image, $elementCategory, $elementID);
					break;
			}
		}
	
	
		// if debug mode is on, send the debug messages and halt here
		if($debugOutputRequested)
			die(ob_end_flush());
	
	
		// tells the caller to expect a PNG image and then sends the image
		ob_end_clean();
		header('Content-Type: image/png');
		imagepng($image);
	}
	catch(DBException $dbException)
	{
		// don't publicly display database queries in order to avoid exposing the database's structure
		throw new Exception('Database Error '.$dbException->getMySQLErrorCode());
		
		/*
		 * comment the above section and use this one instead when debugging
		 *
		if($debugOutputRequested)
			die($dbException->getMessage());
		else
			throw new Exception('Database Error '.$dbException->getMySQLErrorCode());
		*
		*/
	}
}
catch(Exception $exception)
{
	// if debug mode is on, send the debug messages and halt here
	if($debugOutputRequested)
		die('Error: '.$exception->getMessage().'<br /><br />The following messages were also issued:<br />'.ob_get_clean());
	else
		ob_end_clean();
	
	// produce an image with the error message
	$image = generateErrorImage('Error: '.$exception->getMessage());
	header('Content-Type: image/png');
	imagepng($image);
	return;
}

?>
