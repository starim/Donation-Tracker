<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * The following code is based on Paypal's sample code for an IPN listener
 * 
 * This script handles communication with Paypal once an IPN is received to verify the authenticity of the IPN.
 * 
 */


require_once('classes/DBConnection.php');
require_once('classes/IPN.php');
require_once('configuration.php');
require_once('classes/exceptions/IPNProcessorException.php');


if(DEBUG_MODE)
	$paypalServerURL = 'ssl://www.sandbox.paypal.com';
else
	$paypalServerURL = 'ssl://www.paypal.com';
$paypalServerPort = 443;

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

// this will store the text of the original IPN
$IPNText = array();

foreach ($_POST as $key => $value) {
	$IPNText[] = "$key=$value";
	$req.= "&$key=".urlencode($value);
}

// finish the record of the original IPN text
$IPNText = implode("\n", $IPNText);

// post back to PayPal system to validate
$header = "POST /cgi-bin/webscr HTTP/1.0\n";
$header .= "Content-Type: application/x-www-form-urlencoded\n";
$header .= "Content-Length: " . strlen($req) . "\n\n";
$fp = fsockopen ($paypalServerURL, $paypalServerPort, $errno, $errstr, 30);


extract($_POST);

try
{
	// check that all the required information is present, and throw an Exception if not
	
	if(!isset($payment_status))
		throw new IPNProcessorException('Missing payment status (payment_status) in IPN.', $IPNText);
		
	if(!isset($mc_gross))
		throw new IPNProcessorException('Missing gross payment amount (mc_gross) in IPN.', $IPNText);

	// the fee amount is not necessary for processing IPNs and will be assumed to be 0 if it is not given
	if(!isset($mc_fee))
		$mc_fee = 0;
		
	if(!isset($txn_id))
		throw new IPNProcessorException('Missing transaction ID (txn_id) in IPN.', $IPNText);
		
	if(!isset($txn_type))
		throw new IPNProcessorException('Missing transaction type (txn_type) in IPN.', $IPNText);
		
	if(!isset($receiver_id))
		throw new IPNProcessorException('Missing referral number (receiver_id) in IPN.', $IPNText);
		
	if(!isset($item_name))
		throw new IPNProcessorException('Missing name of item purchased/donated toward (item_name) in IPN.', $IPNText);
		
	// I don't know if Paypal always gives a payer's email address, so if it's not there then set it to an empty string
	if(!isset($payer_email))
		$payer_email = '';
	
	// build the IPN object
	$IPN = new IPN($IPNText, $payment_status, $mc_gross, $mc_fee, $txn_id, $txn_type, $receiver_id, $payer_email, $item_name);
	

	if($fp === false)
	{
		// HTTP ERROR
		// log the error and wait for Paypal to resend
		throw new IPNProcessorException("Could not establish a connection to Paypal's server at the URL $paypalServerURL on port $paypalServerPort. The system-level error number is $errno. The error message is $errstr.", $IPNText);
	}
	else
	{
		fputs($fp, $header . $req);
		while(!feof($fp))
		{
			$res = fgets ($fp, 1024);
			if(strcasecmp ($res, "VERIFIED") == 0)
			{
				// now that the IPN has been verified by Paypal, go ahead and process it
				$IPN->logIPN();
			}
			else
			{
				// log that Paypal says this IPN is invalid
				if(strcasecmp ($res, "INVALID") == 0)
					throw new IPNProcessorException('Paypal indicates this IPN is invalid. This IPN was either corrupted in transit, or is a forgery.', $IPNText);
			}
		}
		
		fclose ($fp);
	}
}
catch(IPNProcessorException $exception)
{
	$database = new DBConnection(false);
	$cleanErrorMessage = $database->sanitize($exception->getMessage());
	$cleanIPNText = $database->sanitize($exception->getIPNText());
	$database->query("INSERT INTO ".DBConnection::$errorsTable." SET ErrorMessage='$cleanErrorMessage', IPNText='$cleanIPNText', Timestamp=NOW()");
}
catch(Exception $exception)
{
	$database = new DBConnection(false);
	$cleanErrorMessage = $database->sanitize($exception->getMessage());
	$database->query("INSERT INTO ".DBConnection::$errorsTable." SET ErrorMessage='$cleanErrorMessage', IPNText=NULL, Timestamp=NOW()");
}

?>
