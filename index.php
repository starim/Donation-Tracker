<?php
	// redirect the visitor to the Donation Manager web interface

	$standardPort = null;
	// $protocol = strstr($_SERVER['SERVER_PROTOCOL'], '/', true);		only works in PHP 5.3, which not all web hosts use yet
	$position = strpos($_SERVER['SERVER_PROTOCOL'], '/');
	$protocol = substr($_SERVER['SERVER_PROTOCOL'], 0, $position);
	if(strcasecmp($protocol, 'http') === 0)
		$standardPort = 80;
	else if(strcasecmp($protocol, 'https') === 0)
		$standardPort = 443;
	
	$protocolString = strtolower($protocol).'://';
	$domainNameString = $_SERVER['SERVER_NAME'];
	$portString = (!is_null($standardPort) && ($_SERVER['SERVER_PORT'] != $standardPort) ? ':'.$_SERVER['SERVER_PORT'] : '');
	$pathString = $_SERVER['REQUEST_URI'].'web_interface_support/';
	$fileNameString = 'login.php';
	
	$URL = $protocolString.$domainNameString.$portString.$pathString.$fileNameString;

	header("Location: $URL", true, 301);
?>
