== Donation Manager ==
Version 1.2-development (unstable)
© 2009 Brent Houghton

This is free software under version 3 or later of the Affero General Public
License (AGPL); see LICENSE.txt for the full text of this license.

Source code for this project is freely available at
	github.com/starim/Donation-Tracker

Note that the license obligates users of this software to make the source code
for any modifications or extensions to the software freely available.


Author Contact Information:
	Brent Houghton
	slixbits@gmail.com


Requirements:
	a web server
	PHP 5.0 or later
	MySQL 5.0.17 or later


What is the Donation Manager?

This project originally began as a way for a webcomic author to use Paypal's 
Instant Payment Notification (IPN) service to track payments received and 
display progress counters toward donation goals on their website. While built 
with an online comic in mind--where new pages for the comic are released
when a certain amount of money has been collected from readers--it is usuable
by anyone wanting to track payment from multiple people working collectively
toward a monetary goal, so the system covers donation drives just as well as
readers contributing toward buying new pages in a comic series.

In addition to keeping a count of money received, the Donation Manager also
provides a system for generating images displaying progress toward the donation
goal. These images are referred to as "donation counters".

For a thorough overview of PayPal's IPN service, visit www.paypal.com/ipn


Can the Donation Manager track money sent via payment processors other than
PayPal?

Currently only PayPal is supported. Payment from other sources can still be
entered manually into the Donation Manager by the site administrator, so the
donation counters can be still be used even if payments aren't automatically
tracked.




Setting Up the Donation Manager:


1. Ensure that all required software is installed and configured:

	a. a web server to accept Paypal's IPNs and run the Donation Manager's web 
		interface
	
	b. PHP 5.0 or later installed on your server
		Additionally, PHP's MySQL library must be installed in order for the 
		PHP scripts to communicate with the MySQL database.
		
	c. MySQL 5.0.17 or later running on your database server


2. Set up the database infrastruce needed by the Donation Manager. SQL
   statements are provided for each step; you can use SQL tools like MySQL
   Workbench (www.mysql.com/downloads/workbench/) to execute these statements
   on your own database.

	a. Create the database schema you entered into the configuration file in the
		previous step, if it doesn't already exist. This SQL will create the
		schema:

CREATE SCHEMA `<schema name>`;

for example:
CREATE SCHEMA `donation_manager`;


	b. Create the user account to be used by the Donation Manager's automatic
		IPN processing and donation counter generator. This account should have
		only SELECT, UPDATE, INSERT, and DELETE privileges in the Donation
		Manager's schema, and no access to any other schemas. This account
		should also only be able to log in from the web server. This SQL will
		create the user account:

GRANT SELECT, UPDATE, INSERT, DELETE ON `<schema name>`.* TO '<user name>'@'<web server domain name or IP address>' IDENTIFIED BY '<password>';
FLUSH PRIVILEGES;

for example:
GRANT SELECT, UPDATE, INSERT, DELETE ON `donation_manager`.* TO 'donation_manager_auto_ops'@'mydomain.com' IDENTIFIED BY 'ಠ0a⌣◍8▲?$@▰x';
FLUSH PRIVILEGES;


	c. Run the database setup script. This is found in the file named
		"current state.sql" found in the "database structure" folder. You can
		copy this script into your SQL editor and execute it directly.


3. Edit configuration.php to provide the necessary settings for connecting to 
   your database:

	DB_USER
	DB_PASSWORD
	These are the username and password, respectively, for the database account
	you created in step 2b. The Donation Manager will use these credentials to
	access the database when logging IPNs and generating donation counters.
	Note that if your password contains the apostrophe ' character, it must be
	entered as \' instead (so the password fgE'lad&sd should be entered into
	the configuration file as fgE\'lad&sd).
	If a default MySQL user name and password are set in PHP's configuration 
	(php.ini) and you want to use those parameters, then set DB_USER and
	DB_PASSWORD to empty strings. However, it is strongly recommended you not
	do this and instead use an account created specifically for the donation
	manager as described above.
	
	DB_SERVER
	DB_PORT
	These are the server name (or IP address) and port used to access the MySQL
	server.
	If a default MySQL server is set in PHP's configuration file (php.ini) and 
	you want to use those parameters, then set DB_SERVER and DB_PORT to 
	empty strings.
	
	DB_SCHEMA
	The name of the schema the Donation Manager should use. The system will 
	create the tables it needs in this schema, so the schema must already 
	exist before the Donation Manager is first used.


4. Set up your web server to host the contents of the Donation Manager. The 
   details of this step depend on your web server, but if you are on a typical 
   web host then simply placing the files on the server via FTP is usually 
   sufficient.


5. Visit the donation_manager.php page in a browser to use the Donation 
   Manager's web interface for the first time.
   
	a. Logging In
		Instead of creating its own account system, the Donation Manager 
		uses the database's set of user accounts for authentication. This means
		that logging in to the Donation Manager requires providing credentials
		for a database user account with access to the Donation Manager's
		schema. This also means that if in the future you wish to give someone
		else limited access to the Donation Manager (for example, letting them
		look at settings but not the ability to change any settings) then
		simply create a database user account with appropriately restricted
		access (read-only access in the example here).
	
	b. Configuring the Donation Manager's Behavior
		Once logged in, the Donation Manager will run its setup process to 
		finalize the system setup. It will ask for the following configuration
		information:
		
		Payment Calculation Mode: Net or Gross
		
			Determines how much money from each transaction should be counted 
			toward donation targets. Net mode counts all money that remains 
			after taxes and Paypal fees have been deducted from each donation, 
			while Gross mode counts the full amount given by the donor for
			each transaction.
			Note that changing this setting later is not retroactive--only 
			future donations are affected by this setting.
		
		Referral ID:
		
			Your PayPal referral ID (sometimes called the receiver ID). This is
			a number (typically 13 digits) assigned by PayPal to uniquely 
			identify each merchant. Having this number allows the Donation 
			Manager to more reliably validate each received IPN, but if you 
			don't know your referral ID or don't care to provide it you can 
			simply leave this field blank.
		
		Note that these settings can be changed later via the web interface 
		(use the Edit Options button at the bottom of the page after setting up
		the system is complete).
	

6. Use the web interface to set up your items and donation counters.

	a. Items are organized into categories. Create a category by clicking the 
		Create New Category Button near the bottom left of the page.
	
	b. Once you've created a category, you can use the selector on the left 
		side of the page to choose it for editing. A list of items in this 
		category is shown, with an option at the bottom of the list to add a 
		new item.

	c. Create your first item using the control at the bottom of the (currently
		empty) item list. You will have to choose a name for the item as well
		as pick which blueprint should be used for this item's donation
		counter.
	
		Unfortunately the web interface does not currently provide a way to 
		customize or add new donation counter blueprints so you are limited to 
		using the built-in donation counter styles. The style for an item's 
		donation counter is pick during item creation.

		If you are familiar with SQL and take some time to understand the 
		Donation Manager code, then you will be able to create your own counter
		blueprints by directly accessing the database and adding the 
		configuration directly into the Donation_Counters_Blueprints and 
		Donation_Counters_Text_Elements tables.
	

7. Visit PayPal's website and turn the Instant Payment Notification feature on 
   for your account, setting IPNs to be sent to the IPN_listener.php script on
   your server. PayPal provides instructions for this step here:
cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_admin_IPNSetup#id089EG030E5Z


Once all these steps are done, the Donation Manager is fully set up and ready 
for use. Whenever you receive payment toward any of the items tracked by the
Donation Manager, the donation totals will be updated automatically on any
donation counters displayed on your website.




Updating the Donation Manager

The Donation Manager's code is hosted on GitHub. Check
	github.com/starim/Donation-Tracker
periodically for updates to the Donation Manager. Stable versions are tagged
(click the Tags button on the page to view a list of these tagged versions).
Only use the tagged versions for production use; the non-tagged versions are
development snapshots and generally not fit for use on a live site.

If a more recent stable version of the Donation Manager is available, you can
update your site's Donation Manager by downloading the latest code from GitHub
(clicking the Tags button will list stable versions you can download). Before
updating, rename your configuration.php file so it won't be overwritten with
the default settings. Now delete all the old code except the old configuration
file off your server and replace it with the new code you just downloaded. Copy
the settings from the old configuration file into the new one, and check if any
new settings have shown up and need to be configured.

Finally, there may be changes to the database, so check the
"database structure/updates/" folder for a list of SQL scripts that will handle
upgrading your database. These update files are organized by version, so the
scripts to update from Donation Manager version 1.0 to 1.1 will kept in their
own folder, the scripts to update from 1.1 to 1.2 in a separate folder, etc.
The update scripts are numbered. Run all update scripts relevant to your
version upgrade in the correct order.

Once both the database and the code on your server are updated, the upgrade is
complete.




Using the Donation Manager

	a. Editing Items

	The Donation Manager's web interface allows you to edit any property of an
	item other than its donation counter. In addition to updating names or
	donation goals, this also allows you to make manual adjustments to the
	donation total if there is income from sources that can't be tracked by the
	Donation Manager. To edit an item's properties,	simply select its category
	in the web interface and the item's properties will be shown; click on any
	displayed property to edit. Changes are saved instantly.

	Additionally, if you discontinue merchandise, set the Active field for the
	item to the red X, deactivating the item. If payment is received for a
	deactivated item, it will be logged as an error instead of being credited
	toward the item's donation total. Items can be deactivated or reactivated at
	any time (though you will need to unclick the Hide Disabled Items option on
	the bottom right side of the web interface to see disabled items to edit
	them).
	

	b. Embedding Donation Counters on Your Site

	Donation counters are simply images and can be placed anywhere an image can 
	go. Typically images are displayed using the HTML <img> tag.
	
	The quickest way to get the HTML to embed a donation counter image on your
	site is to log in to the Donation Manager web interface
	(donation_manager.php) select the category the desired item is in, and
	choose the "Click to Preview" option from the items list. This will open
	a dialog box showing a preview of the donation counter and giving the URL
	for the donation counter (used when embedding the counter in CSS, for
	example as a background image) or the HTML for embedding the counter as an
	<img> element, which is the most common way	to place a donation counter on
	a web page.

	Because the displayed counter image needs to change whenever more money is 
	sent for an item, the counter images are marked as uncacheable for 
	browsers. This has the advantage that users will always see the most up-to-
	date donation amounts for each item when they load a page, but comes with
	the	downside that when a user visits a page every single counter image must
	be generated again, slowing down page load times. The built-in donation
	counter	images are deliberately small (250x59 px) to help minimize load
	times, but care should be taken if making custom counter blueprints to use
	large images sparingly.


	c. Getting Item Status By Script

	If you have scripts on your web pages that need access to donation amounts,
	or you want to display item status in a way other than the donation
	counters, the Donation Manager provides a PHP interface in the
	MerchandiseStatusInterface.php script, located in the public_interface
	folder. Note that this is a read-only interface; there is no scriptable way
	to manipulate the Donation Manager, only get the status of merchandise.

	The following item properties are returned by the interface's getItemData() method:
 
	Category (string): the name of the category to which this item belongs
	Donation Toward Next (floating point number): the dollar amount donated
		toward buying the next copy of this item
	Total Donations (floating point number): the total dollar amount donated
		for this item
	Copies Owed (integer): the number of copies owed for this item
	Copies Done (integer): the number of copies given out for this item
	Price Per Copy (floating point number): the dollar price for each copy of
		this item
	Max Copies Available (integer or null): the maximum number of copies
		available for limited purchase items, or null for unlimited purchase items
	Donation Goal Reached (boolean or null): whether the item has reached its
		maximum donation amount for limited purchase items, or null for unlimited purchase items
	Is Active (boolean): whether this item is marked as active (in the Donation
		Manager items can be deactivated so that they stop accepting payments, which is useful for discontinued items)
 
	Sample usage:

<?php

require_once('<path to Donation Manager>/public_interface/MerchandiseStatusInterface.php');

try
{
	// initialize the interface
	$itemData = new MerchandiseStatusInterface();
}
catch(Exception $exception)
{
	// handle the Donation Manager failing to initialize
}

// get a list of the items tracked by the Donation Manager
$itemsList = $itemData->getItemList();

// get all the properties for an item named "Item #1"
$item1Properties = $itemData->getItemData('Item #1');
if(is_null($item1Properties))
	echo 'Error: no item named "Item #1" exists in the Donation Manager.'."\n";
else
	echo 'Item #1 properties: '.print_r($item1Properties, true)."\n";

// display the progress of "Item #2" toward its donation goal
$item2Properties = $itemData->getItemData('Item #2');
if(is_null($item2Properties))
	echo 'Error: no item named "Item #2" exists in the Donation Manager.';
else if(is_null($item2Properties['Donation Goal Reached']))
	echo 'Item #2 has an unlimited number of copies available so it does not have a donation goal.';
else
{
	$donationGoal = '$'.number_format($item2Properties['Max Copies Available'] * $item2Properties['Price Per Copy'], 2);
	
	if($item2Properties['Donation Goal Reached'])
		echo "Item #2 has reached its donation goal of $donationGoal!\n";
	else
		echo "Item #2 donation progress: $".$item2Properties['Total Donations']." / $donationGoal\n";
}

?>




Limitations

	There is no functionality in the web interface at present to create or modify
	donation counter blueprints.	

	The system cannot track money amounts larger than $999,999,999.99
	
	The Donation Manager is localized to United States, so the interface is
	entirely in English and payments are assumed to be in US dollars. There is no
	infrastructure in place for internationalization at this time.
	
