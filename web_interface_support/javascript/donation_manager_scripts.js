/**
 * @author: Brent Houghton © 2010
 * 
 * This Javascript file contains scripts for handling the interactive content found on the donation manager page.
 * 
 */


"use strict";


if(typeof(DEBUG_MODE) == "undefined")
	var DEBUG_MODE = false;


// set default AJAX settings
$.ajaxSetup({
	"async": "true",
	"url": "web_interface_support/donation_manager_support.php",
	"type": "POST",
	"cache": "false",
	"dataType": "json",
	"timeout": 5000,
	"crossDomain": false,
	"error": function(jqXHR, textStatus, errorThrown) {
	
		var errorString = "Communication with the server failed: " + textStatus;
		if(DEBUG_MODE)
		{
			errorString += "\nResponse from server: " + jqXHR.responseText;
			if(typeof(errorThrown) == "string")
				errorString += "\nError details: " + errorThrown;
		}
		
		alert(errorString);
	}
});


// checks whether there are any IPN processor errors recorded in the Errors table in the database
// since this function uses an asynchronous AJAX request, it retuns nothing; use the callback instead
// takes a callback executed when a response is received from the server; the callback should take a single boolean
// parameter indicating whether there are any IPN processor errors recorded in the Errors table in the database
function getIPNProcessorErrorsExist(callback)
{
	var output = false;
	$.ajax({
		"data": {
			"action": "checkErrorsExist",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
				output = response["errorsExist"];
		},
		"complete": function(jqXHR, textStatus) {
			callback(output);
		}
	});
}


// checks whether there are any IPNs recorded in the IPNs table in the database
// since this function uses an asynchronous AJAX request, it retuns nothing; use the callback instead
// takes a callback executed when a response is received from the server; the callback should take a single boolean
// parameter indicating whether there are any IPNs recorded in the IPNs table in the database
function getIPNsExist(callback)
{
	var output = false;
	$.ajax({
		"data": {
			"action": "checkIPNsExist",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
				output = response["IPNsExist"];
		},
		"complete": function(jqXHR, textStatus) {
			callback(output);
		}
	});
}


// checks whether the expected database tables exist
// since this function uses an asynchronous AJAX request, it retuns nothing; use the callback instead
// takes a callback executed when a response is received from the server; the callback should take a single boolean
// parameter indicating whether the expected database tables exist
function getIsDonationManagerSetUp(callback)
{
	var output = false;
	$.ajax({
		"data": {
			"action": "isDBSetUp",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
				output = response["isDBSetUp"];
			else
				alert("Error while checking whether the Donation Manager database infrastructure is set up: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			callback(output);
		}
	});
}


// checks whether the Donation Manager options have been configured yet
// since this function uses an asynchronous AJAX request, it retuns nothing; use the callback instead
// takes a callback executed when a response is received from the server; the callback should take a single boolean
// parameter indicating whether the Donation Manager options have been configured yet
function getIsDonationTrackerConfigured(callback)
{
	// default to assuming the database is in place, since assuming otherwise would mean running the configuration script and possible overwriting an existing configuration
	var output = true;
	$.ajax({
		"data": {
			"action": "isDBConfigured",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
				output = response["isDBConfigured"];
			else
				alert("Error while checking whether the donation tracker database is configured: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			callback(output);
		}
	});
}


// gets the list of donation counter blueprints and fills in corresponding options to the item creation control
// the simpleCountersOnly parameter is a boolean that indicates whether to only show blueprints compatible with
// simple counters
function initializeBlueprintsList(simpleCountersOnly)
{
	$.ajax({
		"data": {
			"action": "getCounterBlueprintList",
			"simpleCountersOnly": simpleCountersOnly,
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				if(response["blueprint list"].length > 0)
				{
					var selectControl = $("#itemCreationPopup #newItemBlueprintContainer select").html("");
			
					for(var i in response["blueprint list"])
					{
						var blueprintName = response["blueprint list"][i];
						$("<option></option>").attr("value", blueprintName).text(blueprintName).appendTo("#itemCreationPopup #newItemBlueprintContainer select");
					}
				}
				else
				{
					var noCountersExistOption = $("<option></option>").attr("value", "").text("No Counter Blueprints in the Database");
					$("#itemCreationPopup #newItemBlueprintContainer select").html(noCountersExistOption);
				}
			}
			else
				alert("Error while getting the list of available donation counter blueprints from the database: " + response["message"]);
		}
	});
}


// shows or hides the category creation popup window depending on the value of visibility
function categoryCreationPopupVisibility(visibility)
{
	if(visibility)
	{
		// initializes the display
		$("#categoryCreationPopup #createCategorySubmit").attr("disabled", false);
		$("#categoryCreationPopup #newCategoryNameContainer input").val("");
		$("#categoryCreationPopup #newCategoryPaymentThresholdContainer input").val("");
		$("#categoryCreationPopup #newCategoryMaxItemsAllowedContainer input").val("");
		$("#newCategorySimpleCountersOnly").attr("checked", "checked");
		categoryCreation_simpleCountersOptionSelected();
		
		// makes the popup visible
		$("#categoryCreationPopup").dialog("open");
		
		// gives focus to the first input element
		// this WILL NOT WORK if placed before the function that makes the popup appear
		$("#categoryCreationPopup #newCategoryNameContainer input").focus();
	}
	else
	{
		$("#categoryCreationPopup").dialog("close");
	}
}


// enables/disables inputs in the category creation popup as appropriate for the selected option
function categoryCreation_simpleCountersOptionSelected()
{
	$("#newCategoryPaymentThresholdContainer").addClass("disabled");
	$("#newCategoryMaxItemsAllowedContainer").addClass("disabled");
	$("#newCategoryPaymentThresholdContainer input").attr("disabled", true);
	$("#newCategoryMaxItemsAllowedContainer input").attr("disabled", true);
}


// enables/disables inputs in the category creation popup as appropriate for the selected option
function categoryCreation_limitedPagesOptionSelected()
{
	$("#newCategoryPaymentThresholdContainer").removeClass("disabled");
	$("#newCategoryMaxItemsAllowedContainer").removeClass("disabled");
	$("#newCategoryPaymentThresholdContainer input").attr("disabled", false);
	$("#newCategoryMaxItemsAllowedContainer input").attr("disabled", false);
}


// enables/disables inputs in the category creation popup as appropriate for the selected option
function categoryCreation_unlimitedPagesOptionSelected()
{
	$("#newCategoryPaymentThresholdContainer").removeClass("disabled");
	$("#newCategoryMaxItemsAllowedContainer").addClass("disabled");
	$("#newCategoryPaymentThresholdContainer input").attr("disabled", false);
	$("#newCategoryMaxItemsAllowedContainer input").attr("disabled", true);
}


// creates a new item category, or displays a helpful error message if the user incorrectly entered values
function createCategory()
{
	// disable the Create Category button to prevent multiple submissions
	$("#categoryCreationPopup #createCategorySubmit").attr("disabled", true);
	
	var categoryType = $("#categoryCreationPopup input[name='newCategoryType']:checked").val();
	if(typeof(categoryType) == "undefined")
	{	
		alert("You must select which type of category to create.");
		$("#categoryCreationPopup #createCategorySubmit").attr("disabled", false);
		return;
	}

	$.ajax({
		"data": {
			"action": "createCategory",
			"categoryType": categoryType,
			"categoryName": $("#categoryCreationPopup #newCategoryNameContainer input").val(),
			"paymentThreshold": $("#categoryCreationPopup #newCategoryPaymentThresholdContainer input").val(),
			"maxItemsAllowed": $("#categoryCreationPopup #newCategoryMaxItemsAllowedContainer input").val(),
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
			{
				categoryCreationPopupVisibility(false);
				refreshItemCategories();
			}
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			// re-enable the Create Category button
			$("#categoryCreationPopup #createCategorySubmit").attr("disabled", false);
		}
	});
}


// retrieves the list of item categories in the database and replaces the item categories list with the newly fetched list
// WARNING: resets the session ID, so don't use any other AJAX scripts that call PHP functions that alter session data while
// this AJAX request is active 
function refreshItemCategories()
{
	$.ajax({
		"data": {
			"action": "refreshItemCategories",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
				$("#itemCategoryDisplay > div").html(response["itemCategoriesHTML"]);
			else
				alert("An error occurred while trying to load the list of item categories. The error message is:\n\n" + response["message"]);
			
			// update the IDCheck1 variable if possible
			if(typeof(response["IDCheck1"]) != "undefined")
				window.IDCheck1 = response["IDCheck1"];
		}
	});
}


// selects an item category, changing its visual appearance by applying a new CSS class and refreshing the items list
// any selected item categories will be unselected, unless the passed category ID is the same as the currently selected category ID, in which case nothing
// will happen
function selectItemCategory(itemCategoryID)
{
	// don't bother doing anything if the user just selected the already selected item category
	if($("#itemCategoryDisplay > div > div#category_" + itemCategoryID).hasClass("selected"))
		return;
		
	$("#itemCategoryDisplay > div > div.selected").removeClass("selected");
	$("#itemCategoryDisplay > div > div#category_" + itemCategoryID).addClass("selected");
	
	refreshItems();
}


function deleteItemCategory()
{
	// find the selected category
	var numSelectedCategories = $("#itemCategoryDisplay > div > div.selected").length;

	// stop and alert the user if no item category has been selected yet
	if(numSelectedCategories == 0)
	{
		alert("You must select an item category. Click a category to select it.");
		return;
	}
	
	var categoryName = $("#itemCategoryDisplay div.selected > p.itemCategoryName").html();
		
	if(!confirm("This will PERMANENTLY delete the category \"" + categoryName + "\", all items it contains, and all donation counter blueprints associated with those items.\n\n"
				+ "Are you sure you want to proceed?"))
		return;


	$.ajax({
		"data": {
			"action": "deleteCategory",
			"categoryID": ($("#itemCategoryDisplay > div > div.selected").attr("id").split("_"))[1],
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
			{
				itemsCoverupVisibility(true);
				refreshItemCategories();
			}
			else
				alert(response["message"]);
		}
	});
}


// shows or hides the div covering up the items table depending on the value of visibility (true to show, false to hide)
function itemsCoverupVisibility(visibility)
{
	if(visibility)
		$("#itemsDisplay > div#itemsTableCoverup").fadeIn(500);
	else
		$("#itemsDisplay > div#itemsTableCoverup").fadeOut(750);
}


// returns whether the div covering up the items table is visible
function isItemsCoverupVisible()
{
	return ($("#itemsDisplay > div#itemsTableCoverup").css("display") != "none");
}


// retrieves the list of items in the database associated with the currently selected item category and replaces the items list with the newly fetched list
function refreshItems()
{
	// gets the ID of the currently selected category
	var fullCategoryName = $("#itemCategoryDisplay > div > div.selected").attr("id"); // this will be category_* where * is the category's ID
	var categoryID = fullCategoryName.substr(fullCategoryName.indexOf("_") + 1);


	$.ajax({
		"data": {
			"action": "refreshItems",
			"categoryID": categoryID,
			"showOnlyEnabledItems": $("#itemsDisplay #showOnlyEnabledItemsControl").is(":checked"),
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				// fade out the table, fill with the requested items' data, and then make it fade back in
				$("#itemsDisplay table").fadeOut("fast", function() {
			
					// a helper function that returns an HTML element that controls user interaction with a specific cell in the displayed items table
					// the type parameter determines what type of input is allowed in this cell and can be one of: "text", "positive integer", or
					// "positive floating point"
					// if the passed type parameter is not one of the recognized values, then it defaults to "text"
					// maximum is an optional parameter defining the maximum value this field can hold
					// maximum is only used if the type parameter is numeric ("positive integer" or "positive floating point") and is used to set the
					// range on the generated HTML 5 number input
					var buildControlElement = function(type, maximum) {
						var inputElement;
						switch(type)
						{
							case "positive integer":
								inputElement = $('<input type="number" min="0" step="1">');
								if((maximum) != null && !isNaN(maximum))
									inputElement.attr("max", maximum);
								break;
					
							case "positive floating point":
								inputElement = $('<input type="number" min="0" step="0.01">');
								if((maximum) != null && !isNaN(maximum))
									inputElement.attr("max", maximum);
								break;
					
							case "text":
							default:
								inputElement = $('<input type="text">');
						}
			
						var checkBoxDiv = $("<div></div>")
								.append($("<img>").attr({"src": "web_interface_support/images/agt_action_success.png", "alt": ""}).click(submitItemProperty))
								.append($("<img>").attr({"src": "web_interface_support/images/agt_action_fail.png", "alt": ""}).click(cancelEditItemProperty));
						var controlHTML = $("<div></div>").addClass("itemPropertyEditControl")
									.append(inputElement)
									.append(checkBoxDiv);
				
						return controlHTML;
					}
			
					var itemsTableBody = $("<tbody></tbody>");
					
					var isSimpleCounterCategory = response["is simple counter category"];

					// cycle through each item, giving it a row in the table to display its properties
					var evenRow = false;
					for(var thisItemID in response["items"])
					{
						var thisItemProperties = response["items"][thisItemID];
					
						var thisRow = $("<tr></tr>");
					
						// assign a background style (light vs. dark) and give rows for limited page items which have been maxed out a distinct style
						if((thisItemProperties['MaxItemsAllowed'] != 0) && ((thisItemProperties['ItemsOwed'] + thisItemProperties['ItemsGiven']) >= thisItemProperties['MaxItemsAllowed']))
							thisRow.addClass(evenRow ? "lightMaxedOut" : "darkMaxedOut");
						else
							thisRow.addClass(evenRow ? "light" : "dark");
					
						// rows for disabled items are given a distinct class
						if(!thisItemProperties['Active'])
							thisRow.addClass("disabledItem");

					
						// cycle through each item property, building the cells for this table row
						for(var itemProperty in thisItemProperties)
						{
							// skip these properties as they are provided for use by this script and not for display to the user
							if((itemProperty == "ID") || (itemProperty == "PaymentThreshold") ||(itemProperty == "MaxItemsAllowed"))
								continue;
						
							var value = response["items"][thisItemID][itemProperty];
							
							// the name of the item represented by each row is stored attaced to the row element itself
							if(itemProperty == "Name")
								thisRow.data("item name", value);
						
							var thisCell = $('<td></td>');
						
							// contains the HTML to be inserted into each cell to support scripts that control editing item properties
							// note that not all columns will actually make use of these elements, but most do
							var itemValueElement = $("<div></div>").addClass("itemPropertyValue");
							var valueDisplay = $("<span></span>").text(value);
						
						
							// assemble the components that make up the content of this table cell: an element for displaying the value for this cell, and an element for controlling
							// user interaction
							switch(itemProperty)
							{
								case "Counter URL template":
								
									itemValueElement
										.text("Click to preview.")
										.data("URL template", value)
										.click(function() { counterPreviewPopupVisibility(true, $(this).parents("#itemsDisplay tr").data("item name"), $(this).data("URL template")) });
									thisCell.append(itemValueElement);
									
									break;


								// this column displays an icon representing either "active" or "disabled"
								case "Active":

									var valueTrueDisplay = $("<img>").addClass("true" + (value ? " set" : "")).attr({"src": "web_interface_support/images/true.png", "alt": ""});
									var valueFalseDisplay = $("<img>").addClass("false" + (!value ? " set" : "")).attr({"src": "web_interface_support/images/false.png", "alt": ""});
									itemValueElement.click(function() { editBooleanItemProperty($(this).parent().attr("id")) }).append(valueTrueDisplay).append(valueFalseDisplay);
								
									var controlElement = buildControlElement("text");
								
									thisCell.addClass("smallColumn").addClass("centeredColumn").append(itemValueElement).append(controlElement);
								
									break;

								case "PriceOverride":
								
									// simple counters can't have a price override, but all other item types should build the price override the same way as the donations column
									if(isSimpleCounterCategory)
									{
										thisCell.text("--").addClass("centeredColumn");
										break;
									}

								case "Donations":
							
									// the value can be either a string or a number; if numeric it should be prefaced with a dollar sign
									var isNumericValue = !isNaN(parseFloat(value)) && isFinite(value);
							
									itemValueElement.click(function() { editItemProperty($(this).parent().attr("id")) }).text(isNumericValue ? "$" : "").append(valueDisplay);
								
									var controlElement = buildControlElement("positive floating point");
								
									thisCell.addClass("smallColumn").addClass("centeredColumn").append(itemValueElement).append(controlElement);
								
									break;


								// these columns take a special value if their category's PaymentThreshold is 0
								case "ItemsOwed":
								case "ItemsGiven":

									if(isSimpleCounterCategory)
										thisCell.text("--");
									else
									{
										itemValueElement.click(function() { editItemProperty($(this).parent().attr("id")) }).append(valueDisplay);
								
										var controlElement
										if(thisItemProperties["MaxItemsAllowed"] > 0)
											controlElement = buildControlElement("positive integer", thisItemProperties["MaxItemsAllowed"]);
										else
											controlElement = buildControlElement("positive integer");
								
										thisCell.append(itemValueElement).append(controlElement);
									}
								
									thisCell.addClass("smallColumn").addClass("centeredColumn");
							
									break;


								// this is the HTML used for generic table cells
								default:
									itemValueElement.click(function() { editItemProperty($(this).parent().attr("id")) }).append(valueDisplay);
								
									var controlElement = buildControlElement("text");
								
									thisCell.append(itemValueElement).append(controlElement);
							}
						
							// these elements contain information about the item property contained in this cell
							var itemIDTrackingHTML = $("<div></div>").addClass("itemID").text(thisItemID);
							var propertyNameTrackingHTML = $("<div></div>").addClass("propertyName").text(itemProperty);
							thisCell.append(itemIDTrackingHTML).append(propertyNameTrackingHTML);

							thisCell.appendTo(thisRow);
						}
					
						thisRow.appendTo(itemsTableBody);
					
						evenRow = !evenRow;
					}
				
				
					// add a row at the bottom that allows the creation of new items
					var itemCreationCell = $("<td></td>")
												.attr("colspan", $("#itemsDisplay table thead tr:first-child").children().length)
												.addClass("createNewItem")
												.click(function() { itemCreationPopupVisibility(true, isSimpleCounterCategory); })
												.text("Click here to create a new item.");
					var itemCreationRow = $("<tr></tr>")
												.addClass(evenRow ? "light" : "dark")
												.append(itemCreationCell);
					itemCreationRow.appendTo(itemsTableBody);
				
				
					// assign a unique id to every cell
					itemsTableBody.find("td").attr("id", function (index) {
					  return "td_" + index;
					});
				
				
					// insert the finished table body onto the table on the page
					var itemsTable = $("div#itemsDisplay table");
					itemsTable.find("tbody").remove();
					itemsTable.append(itemsTableBody);
					itemsCoverupVisibility(false);
				
					$("div#itemsDisplay table").fadeIn("slow");
				});
			}
			else
				alert(response["message"]);
		}
	});
}


// shows or hides the popup window for creating new items (visibility is true to show or false to hide the popup)
// simpleCounter is a boolean parameter that should be set to true if a simple counter item is being created
function itemCreationPopupVisibility(visibility, simpleCounter)
{
	if(visibility)
	{
		// initializes the display
		$("#itemCreationPopup #createCategorySubmit").attr("disabled", false);
		$("#itemCreationPopup #newItemNameContainer input").val("");
		$("#itemCreationPopup #newItemBlueprintContainer select").val("");
		
		initializeBlueprintsList(simpleCounter);
		
		// makes the popup visible
		$("#itemCreationPopup").dialog("open");
		
		// gives focus to the first input element
		// this WILL NOT WORK if placed before the function that makes the popup appear
		$("#itemCreationPopup #newItemNameContainer input").focus();
	}
	else
		$("#itemCreationPopup").dialog("close");
}


// creates a new item, or displays a helpful error message if the user incorrectly entered values
function createItem()
{
	// disable the Create Item button to prevent multiple submissions
	$("#itemCreationPopup #createItemSubmit").attr("disabled", true);
	
	var parameters = new Array;
	
	// gets the ID of the currently selected category
	var fullCategoryName = $("#itemCategoryDisplay > div > div.selected").attr("id"); // this will be category_* where * is the category's ID
	var categoryID = fullCategoryName.substr(fullCategoryName.indexOf("_") + 1);


	$.ajax({
		"data": {
			"action": "createItem",
			"categoryID": categoryID,
			"name": $("#itemCreationPopup #newItemNameContainer input").val(),
			"counterBlueprint": $("#itemCreationPopup #newItemBlueprintContainer select").val(),
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
			if(response["status"])
			{
				itemCreationPopupVisibility(false);
			
				// refreshes the items list to include the new item
				refreshItems();
			}
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			// re-enable the Create Item button
			$("#itemCreationPopup #createItemSubmit").attr("disabled", false);
		}
	});
}


// opens the item property editing controls for the specified td element
function editItemProperty(tdID)
{
	// don't allow other item properties to be edited while a changed item property is being submitted to the server
	if(typeof(window.itemPropertyBeingSubmitted) != "undefined" && window.itemPropertyBeingSubmitted)
		return;
	
	// hide any open item edit controls
	if(window.tdIDBeingEdited != null)
	{
		// if the user clicked again on the td being edited then don't hide the editing controls
		if(window.tdIDBeingEdited == tdID)
			return;
		else
			cancelEditItemProperty();
	}
	
	// store the id of the td element being edited
	window.tdIDBeingEdited = tdID;
	
	// fill the current value into the input
	var oldValue = $("#itemsDisplay #" + tdID + " div.itemPropertyValue span").text();
	$("#itemsDisplay #" + tdID + " div.itemPropertyEditControl input").val(oldValue);
	
	// make the edit control visible
	$("#itemsDisplay #" + tdID + " div.itemPropertyValue").fadeOut("fast", function() {
		$("#itemsDisplay #" + tdID + " div.itemPropertyEditControl").fadeIn("slow", function() {
				$("#itemsDisplay #" + tdID + " div.itemPropertyEditControl input").focus();
		});
	});
}


// boolean item properties are handled differently than all other types of item properties
// when they are clicked their state flips and the change is immediately submitted to the database
function editBooleanItemProperty(tdID)
{
	// set the global variables indicating that this td is being edited and submitted to the database
	window.tdIDBeingEdited = tdID;
	window.itemPropertyBeingSubmitted = true;
	$("#itemsDisplay td#" + tdID).addClass("submitting");


	$.ajax({
		"data": {
			"action": "changeItemProperty",
			"itemID": $("#itemsDisplay table > tbody td#" + tdID + " div.itemID").text(),
			"propertyName": $("#itemsDisplay table > tbody td#" + tdID + " div.propertyName").text(),
			"newValue": ($("#itemsDisplay table > tbody td#" + tdID + " div.itemPropertyValue img.true").hasClass("set") ? 0 : 1),
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			var tdID = window.tdIDBeingEdited;
			window.tdIDBeingEdited = null;
			window.itemPropertyBeingSubmitted = false;
		
			if(response["status"])
			{
				if(response["escapedNewValue"] == 0)
				{
					// change the icon to display the image for false
					$("#itemsDisplay #" + tdID + " div.itemPropertyValue img.set").removeClass("set");
					$("#itemsDisplay #" + tdID + " div.itemPropertyValue img.false").addClass("set");
				
					// apply the disabled style to the row
					$("#itemsDisplay #" + tdID).parent().addClass("disabledItem");
				}
				else
				{
					// change the icon to display the image for true
					$("#itemsDisplay #" + tdID + " div.itemPropertyValue img.set").removeClass("set");
					$("#itemsDisplay #" + tdID + " div.itemPropertyValue img.true").addClass("set");
				
					// remove the disabled style from the row
					$("#itemsDisplay #" + tdID).parent().removeClass("disabledItem");
				}
			
				$("#itemsDisplay #" + tdID).removeClass("submitting");
			}
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			$("#itemsDisplay #" + tdID).removeClass("submitting");
		}
	});
}


// closes the currently open item editing controls
function cancelEditItemProperty()
{
	// don't allow the item property control to be closed while it is submitting changes to the server
	if(typeof(window.itemPropertyBeingSubmitted) != "undefined" && window.itemPropertyBeingSubmitted == true)
		return;

	var tdID = window.tdIDBeingEdited;
	window.tdIDBeingEdited = null;
	
	if(tdID == null)
		return;
	else
		$("#itemsDisplay table > tbody td#" + tdID + " div.itemPropertyEditControl").fadeOut("fast", function() {
			$("#itemsDisplay table > tbody td#" + tdID + " div.itemPropertyValue").fadeIn("slow");
		});
}


// submits the new item properties to the database
function submitItemProperty()
{
	var tdID = window.tdIDBeingEdited;
	
	// if the user did not make any changes, treat this the same as if the user had clicked the cancel icon
	var newValue = $("#itemsDisplay table > tbody td#" + tdID + " div.itemPropertyEditControl input").val();
	if(newValue == unescape($("#itemsDisplay table > tbody td#" + tdID + " div.itemPropertyValue span").html()))
	{
		cancelEditItemProperty();
		return;
	}
	
	// set the global variable indicating that this td is being submitted to the database
	window.itemPropertyBeingSubmitted = true;
	$("#itemsDisplay table > tbody td#" + tdID).addClass("submitting");
	

	$.ajax({
		"data": {
			"action": "changeItemProperty",
			"itemID": $("#itemsDisplay table > tbody td#" + tdID + " div.itemID").text(),
			"propertyName": $("#itemsDisplay table > tbody td#" + tdID + " div.propertyName").text(),
			"newValue": newValue,
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			var tdID = window.tdIDBeingEdited;
			window.itemPropertyBeingSubmitted = false;
		
			if(response["status"])
			{
				if(response["simpleResponse"])
				{
					var thisCell = $("#itemsDisplay td#" + tdID);
					// fill in the new value, then return this td to its normal state
					thisCell.find(".itemPropertyValue span").text(response["newValue"]);
					thisCell.removeClass("submitting");
					
					// update the item name data stored in the tr element if the item name was changed
					var itemProperty = thisCell.children(".propertyName").text();
					if(itemProperty == "Name")
						thisCell.parents("#itemsDisplay tr").data("item name", response["newValue"]);
					
					cancelEditItemProperty();
				}
				else
				{
					// fill in the new values for all updated columns, then return this td to its normal state
				
					// gets a reference to the spans containing the value for each property of the current item
					var itemProperties = $("#itemsDisplay table > tbody td#" + tdID).parent().find("div.itemPropertyValue span");
				
					// iterates over each updated item property, inserting the new value
					// note that this logic CANNOT handle updating boolean item properties (of which Active is currently the only one)
					var newValues = response["newValue"];
					for(var i in newValues)
						itemProperties.eq(i).text(newValues[i]);

					$("#itemsDisplay table > tbody td#" + tdID).removeClass("submitting");
					cancelEditItemProperty();
				}
			}
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			$("#itemsDisplay #" + tdID).removeClass("submitting");
		}
	});
}


// shows or hides the popup window for previewing the donation counter for an item
// visibility sets whether the popup should be shown or hidden (true to show, false to hide)
// itemName is the name of the item being previewed and is shown in the dialog's title
// URLTemplate is the URL template for the donation counter for this item (the URL with the
// 		item's name replaced by the string %itemName%)
function counterPreviewPopupVisibility(visibility, itemName, URLTemplate)
{
	var popupElement = $("#counterPreviewPopup");
	
	if(visibility)
	{
		var URL = URLTemplate.replace("%itemName%", encodeURIComponent(itemName));
		// initializes the display
		popupElement.dialog("option", "title", 'Donation Counter for "' + itemName + '"');
		popupElement.find("#counterPreviewPopup_counterURL > *").text(URL);
		popupElement.find("#counterPreviewPopup_counterImageHTML  > *").text('<img src="' + URL + '" alt="counter failed to load" />');
		popupElement.find("#counterPreviewPopup_counterImage").attr("src", URL);
		
		// makes the popup visible
		popupElement.dialog("open");
	}
	else
		popupElement.dialog("close");
}


// shows or hides the program options popup depending on the value of visibility
function programOptionsPopupVisibility(visibility)
{
	if(visibility)
		$("#programOptionsPopup").dialog("open");
	else
		$("#programOptionsPopup").dialog("close");
}


// enables/disables inputs in the program options popup as appropriate for the selected option
function programOptions_skipReferralIDOptionSelected()
{
	$("#programOptionsPopup input#programOptions_referralIDInput").attr("disabled", true);
}


// enables/disables inputs in the program options popup as appropriate for the selected option
function programOptions_inputReferralIDOptionSelected()
{
	$("#programOptionsPopup input#programOptions_referralIDInput").attr("disabled", false);
}


// prepares the program options popup window and then shows it
function showProgramOptionsPopup()
{
	$.ajax({
		"data": {
			"action": "loadProgramOptions",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				window.programOptions = new Array();
				window.programOptions["paymentCalculationMode"] = response["optionsMap"]["PAYMENT_CALCULATION_MODE"];
				window.programOptions["referralID"] = response["optionsMap"]["MY_REFERRAL_ID"];

				// initialize the payment calculation mode control
				switch(window.programOptions["paymentCalculationMode"])
				{
					case "gross":
						$("#programOptionsPopup input#programOptions_paymentCalculationGrossOption").attr("checked", "checked");
						break;
					
					case "net":
					default:
						$("#programOptionsPopup input#programOptions_paymentCalculationNetOption").attr("checked", "checked");
						break;
				}
				
			
				// initialize the referral ID control
				if(window.programOptions["referralID"] == "")
				{
					$("#programOptionsPopup input#programOptions_skipReferralIDOption").attr("checked", "checked");
					programOptions_skipReferralIDOptionSelected();
					$("#programOptionsPopup input#programOptions_referralIDInput").val("");
				}
				else
				{
					$("#programOptionsPopup input#programOptions_inputReferralIDOption").attr("checked", "checked");
					programOptions_inputReferralIDOptionSelected();
					$("#programOptionsPopup input#programOptions_referralIDInput").val(window.programOptions["referralID"]);
				}
			}
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			programOptionsPopupVisibility(true);
		}
	});
}


// changes the program's options to the selected settings in the change program options popup window 
function changeProgramOptions() {

	// disable the Done button to prevent multiple submissions
	$("#programOptionsPopup #programOptions_applyChangesButton").attr("disabled", true);
	
	// get the payment calculation mode in the popup window
	var paymentCalculationMode = $("#programOptionsPopup input[name='paymentCalculationMode']:checked").val();
	
	// get the referral ID for which this system will process IPNs, or store an empty string if none was specified
	var referralID;
	if($("#programOptionsPopup #programOptions_referralIDInput").attr("disabled"))
		referralID = "";
	else
		referralID = $("#programOptionsPopup #programOptions_referralIDInput").val();
		
	// make the referral ID upper case to enable case-insensitive comparisons in the Javascript
	referralID = referralID.toUpperCase();
		
	// check which, if any, settings need updating
	var updatePaymentCalculationMode = (paymentCalculationMode != window.programOptions["paymentCalculationMode"]);
	var updateReferralID = (referralID != window.programOptions["referralID"]);
	
	// if there were no changes, just hide the program options popup and return
	if(!(updatePaymentCalculationMode || updateReferralID))
	{
		programOptionsPopupVisibility(false);
		
		// re-enable the Done button
		$("#programOptionsPopup #programOptions_applyChangesButton").attr("disabled", false);
		
		return;
	}

	var options = {};
	if(updatePaymentCalculationMode)
		options["PAYMENT_CALCULATION_MODE"] = paymentCalculationMode;
	if(updateReferralID)
		options["MY_REFERRAL_ID"] = referralID;


	$.ajax({
		"data": {
			"action": "changeProgramOptions",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2,
			"options": options
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
				programOptionsPopupVisibility(false);
			else
				alert("Error: " + response["message"]);
		},
		"complete": function(jqXHR, textStatus) {
			// re-enable the Done button
			$("#programOptionsPopup #programOptions_applyChangesButton").attr("disabled", false);
		}
	});
}


// shows or hides the IPN popup depending on the value of visibility
function IPNLogPopupVisibility(visibility)
{
	if(visibility)
		$("#IPNLogPopup").dialog("open");
	else
		$("#IPNLogPopup").dialog("close");
}


// intializes and displays the IPN log popup to the user
function showIPNLogPopup()
{
	$.ajax({
		"data": {
			"action": "refreshIPNList",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				// if there are no IPNs, state this and return
				if(response["numIPNs"] == 0)
				{
					alert("There are currently no recorded IPNs.");
					return;
				}
			
			
				// fill in the IPN data
			
				$("#IPNLogPopup #IPNLog_currentIPNNum").val("1");
				$("#IPNLogPopup #IPNLog_numIPNs").html(response["numIPNs"]);
			
				var IPNHTML = "";
				var IPNIndex = 1;
				for(var i in response["IPNList"])
				{
					var IPNDetails = response["IPNList"][i];
			
					var IPNDiv =
					'\n<div id="IPNLog_IPN_' + IPNIndex + '" class="IPNDisplay' + (IPNIndex == 1 ? " visible" : "") + '">\n' +
						'<span class="IPNDBIndex">' + i + '</span>\n' +
						'<span class="IPNDisplayIndex">' + IPNIndex + '</span>\n' +
						'<p>Item: <span class="itemName">' + IPNDetails["ItemName"] + " [" + IPNDetails["CategoryName"] + "]" + '</span></p>\n' +
						'<p>Amount: <span class="amount">$' + IPNDetails["Amount"] + '</span></p>\n' +
						'<p>Transaction Type: <span class="transactionType">' + IPNDetails["TransactionType"] + '</span></p>\n' +
						'<p>Time Received: <span class="timestamp">' + IPNDetails["Timestamp"] + ' [server time]</span></p>' +
						'<p>IPN Text:<p class="IPNText">' + IPNDetails["OriginalText"] + '</p></p>\n' +
					'</div>\n';
				
					// add this IPN div to the list
					IPNHTML += IPNDiv;
					IPNIndex++;
				}
			
				$("#IPNLogPopup #IPNLog_IPNDisplayList").html(IPNHTML);


				// show the popup
				IPNLogPopupVisibility(true);
			}
			else
				alert("Error: " + response["message"]);
		}
	});
}


function IPNLog_advance()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#IPNLogPopup #IPNLog_IPNDisplayList .IPNDisplay.visible .IPNDisplayIndex").html());
	var maxIndex = parseInt($("#IPNLogPopup #IPNLog_numIPNs").html());
	
	// don't advance past past the last IPN
	if(currentIndex >= maxIndex)
		return;
	
	// update the display with the new IPN
	
	var nextIndex = currentIndex + 1;
	
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + currentIndex).removeClass("visible");
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + nextIndex).addClass("visible");

	$("#IPNLogPopup #IPNLog_currentIPNNum").val(nextIndex);
}


function IPNLog_advanceToEnd()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#IPNLogPopup #IPNLog_IPNDisplayList .IPNDisplay.visible .IPNDisplayIndex").html());
	var maxIndex = parseInt($("#IPNLogPopup #IPNLog_numIPNs").html());
	
	// exit here if nothing needs to be done
	if(currentIndex == maxIndex)
		return;
	
	// update the display with the new IPN
	
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + currentIndex).removeClass("visible");
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + maxIndex).addClass("visible");

	$("#IPNLogPopup #IPNLog_currentIPNNum").val(maxIndex);
}


function IPNLog_backup()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#IPNLogPopup #IPNLog_IPNDisplayList .IPNDisplay.visible .IPNDisplayIndex").html());
	
	// don't advance past past the last IPN
	if(currentIndex <= 1)
		return;
	
	// update the display with the new IPN
	
	var previousIndex = currentIndex - 1;
	
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + currentIndex).removeClass("visible");
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + previousIndex).addClass("visible");

	$("#IPNLogPopup #IPNLog_currentIPNNum").val(previousIndex);
}


function IPNLog_backupToBeginning()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#IPNLogPopup #IPNLog_IPNDisplayList .IPNDisplay.visible .IPNDisplayIndex").html());
	
	// exit here if nothing needs to be done
	if(currentIndex == 1)
		return;
	
	// update the display with the new IPN
	
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + currentIndex).removeClass("visible");
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_1").addClass("visible");

	$("#IPNLogPopup #IPNLog_currentIPNNum").val(1);
}


// this is meant to be called when a key is pressed while the IPN select input has focus
// if the user presses Enter, displays the IPN whose index is specified
function IPNLog_advanceToSpecificIPN(e)
{
	// get the ASCII code for the character that was pressed
	var charCode = (e.which) ? e.which : event.keyCode;

    // if Enter wasn't pressed, let the keystroke be added to the input
    if(charCode != 13)
    	return true;
    	
    // advance to the specified IPN
    var newIndex = parseInt($("#IPNLogPopup #IPNLog_currentIPNNum").val());
    var maxIndex = parseInt($("#IPNLogPopup #IPNLog_numIPNs").html());
    
    // check that the specified IPN exists before advancing to it
    if(newIndex < 1 || newIndex > maxIndex)
    	return false;
    	
    // advance to the specified IPN
    $("#IPNLogPopup #IPNLog_IPNDisplayList .IPNDisplay.visible").removeClass("visible");
	$("#IPNLogPopup #IPNLog_IPNDisplayList #IPNLog_IPN_" + newIndex).addClass("visible");
    
    return false;
}


// shows or hides the error popup depending on the value of visibility
function errorLogPopupVisibility(visibility)
{
	if(visibility)
		$("#errorLogPopup").dialog("open");
	else
		$("#errorLogPopup").dialog("close");
}


// intializes and displays the error log popup to the user
function showErrorLogPopup()
{
	$.ajax({
		"data": {
			"action": "refreshErrorList",
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				// if there are no errors, state this and return if no error was requested
				// if a specific error was requested, hide the popup and disable the errors button
				if(response["numErrors"] == 0)
				{
					$("#showErrorLogButton").attr("disabled", "disabled");
					$("#showErrorLogButton").addClass("ui-state-disabled");
					alert("There are currently no recorded IPN Processor errors.");
					errorLogPopupVisibility(false);
					return;
				}
			
				// fill in the error data
			
				$("#errorLogPopup #errorLog_currentErrorNum").val(1);
				$("#errorLogPopup #errorLog_numErrors").text(response["numErrors"]);
			
				var errorHTML = "";
				var errorIndex = 1;
				for(var i in response["errorList"])
				{
					var errorDetails = response["errorList"][i];
			
					var errorDiv =
					'\n<div id="errorLog_error_' + errorIndex + '" class="errorDisplay' + (errorIndex == 1 ? " visible" : "") + '">' +
						'<span class="errorDBIndex">' + i + '</span>' +
						'<span class="errorDisplayIndex">' + errorIndex + '</span>' +
						'<p>Error Message: <span class="errorMessage">' + errorDetails["ErrorMessage"] + '</span></p>' +
						'<p>Time logged: <span class="timestamp">' + errorDetails["Timestamp"] + ' [server time]</span></p>' +
						'<p>IPN Text:<p class="errorIPNText">' + errorDetails["IPNText"] + '</p></p>' +
					'</div>\n';
				
					// add this error div to the list
					errorHTML += errorDiv;
					errorIndex++;
				}
			
				$("#errorLogPopup #errorLog_errorDisplayList").html(errorHTML);


				// show the popup
				errorLogPopupVisibility(true);
			}
			else
				alert("Error: " + response["message"]);
		}
	});
}


function errorLog_advance()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .errorDisplay.visible .errorDisplayIndex").text());
	var maxIndex = parseInt($("#errorLogPopup #errorLog_numErrors").text());
	
	// don't advance past past the last error
	if(currentIndex >= maxIndex)
		return;
	
	// update the display with the new error
	
	var nextIndex = currentIndex + 1;
	
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + currentIndex).removeClass("visible");
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + nextIndex).addClass("visible");

	$("#errorLogPopup #errorLog_currentErrorNum").val(nextIndex);
}


function errorLog_advanceToEnd()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .errorDisplay.visible .errorDisplayIndex").text());
	var maxIndex = parseInt($("#errorLogPopup #errorLog_numErrors").text());
	
	// exit here if nothing needs to be done
	if(currentIndex == maxIndex)
		return;
	
	// update the display with the new error
	
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + currentIndex).removeClass("visible");
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + maxIndex).addClass("visible");

	$("#errorLogPopup #errorLog_currentErrorNum").val(maxIndex);
}


function errorLog_backup()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .errorDisplay.visible .errorDisplayIndex").text());
	
	// don't advance past past the last error
	if(currentIndex <= 1)
		return;
	
	// update the display with the new error
	
	var previousIndex = currentIndex - 1;
	
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + currentIndex).removeClass("visible");
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + previousIndex).addClass("visible");

	$("#errorLogPopup #errorLog_currentErrorNum").val(previousIndex);
}


function errorLog_backupToBeginning()
{
	// beware of the int/string ambiguity here!
	var currentIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .errorDisplay.visible .errorDisplayIndex").text());
	
	// exit here if nothing needs to be done
	if(currentIndex == 1)
		return;
	
	// update the display with the new error
	
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + currentIndex).removeClass("visible");
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_1").addClass("visible");

	$("#errorLogPopup #errorLog_currentErrorNum").val(1);
}


// this is meant to be called when a key is pressed while the error select input has focus
// if the user presses Enter, displays the error whose index is specified
function errorLog_advanceToSpecificError(e)
{
	// get the ASCII code for the character that was pressed
	var charCode = (e.which) ? e.which : event.keyCode;

    // if Enter wasn't pressed, let the keystroke be added to the input
    if(charCode != 13)
    	return true;
    	
    // advance to the specified error
    var newIndex = parseInt($("#errorLogPopup #errorLog_currentErrorNum").val());
    var maxIndex = parseInt($("#errorLogPopup #errorLog_numErrors").text());
    
    // check that the specified error exists before advancing to it
    if(newIndex < 1 || newIndex > maxIndex)
    	return false;
    	
    // advance to the specified error
    $("#errorLogPopup #errorLog_errorDisplayList .errorDisplay.visible").removeClass("visible");
	$("#errorLogPopup #errorLog_errorDisplayList #errorLog_error_" + newIndex).addClass("visible");
    
    return false;
}


// erases the specified error from the database and refreshes the error display
function errorLog_deleteDisplayedError()
{
	var errorDBIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .visible .errorDBIndex").text());
	var errorDisplayIndex = parseInt($("#errorLogPopup #errorLog_errorDisplayList .visible .errorDisplayIndex").text());
	
	$.ajax({
		"data": {
			"action": "deleteError",
			"errorIndex": errorDBIndex,
			"IDCheck1": window.IDCheck1,
			"IDCheck2": window.IDCheck2
		},
		"success": function(response, textStatus, jqXHR) {
		
			if(response["status"])
			{
				var numErrors = parseInt($("#errorLogPopup #errorLog_numErrors").text());
				var currentErrorNum = parseInt($("#errorLog_currentErrorNum").val());
				if(numErrors == 1)
				{
					$("#showErrorLogButton").attr("disabled", "disabled");
					$("#showErrorLogButton").addClass("ui-state-disabled");
					errorLogPopupVisibility(false);
				}
				else
				{
					$("#errorLogPopup #errorLog_numErrors").text(--numErrors);
					errorLog_backup()
				}
			}	
			else
				alert("Error: " + response["message"]);
		}
	});
}


// returns true if a digit or a key associated with editing (i.e. backspace) was entered, false otherwise
// call this function in the onkeypress() or onkeyup() event handlers for inputs to only allow digits and
// optionally a period or a dash to be entered
//
// e is the event associated with the key press
//
// currentValue is the current value of the input field (before the current key
// press)
//
// allowPeriod determines whether the period is allowed (for use with floating point numbers) or disallowed
// (for integers)--regardless of the value of this parameter, only one period is allowed to be input in this
// field. Defaults to false.
//
// allowDash determines whether the dash is allowed (for numbers that can be negative) or disallowed (for
// numbers that can only be positive)--regardless of the value of this parameter, a dash will not be allowed
// beyond the first character of the input. Defaults to false.
function isDigitOrEditingKey(e, currentValue, allowPeriod, allowDash)
{
	if(allowPeriod == null)
		allowPeriod = false;
		
	if(allowDash == null)
		allowDash = false;
		
	// get the ASCII code for the character that was pressed
	var charCode = (e.which) ? e.which : event.keyCode;
	
	// check the period as a special case
	if(charCode == 46)
		return (allowPeriod && (currentValue.indexOf(".") == -1));
	
	// check the dash as a special case
	if(charCode == 45)
		return (allowDash && (currentValue.length == 0));

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
