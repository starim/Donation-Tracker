<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script processes login requests from the login page.
 * 
 */


// HTTP/1.1 header to prevent browser caching
header('Cache-Control: no-store, no-cache, must-revalidate');

require_once(dirname(__FILE__) . '/../classes/session.php');
require_once(dirname(__FILE__) . '/../configuration.php');


// call this function if login checks fail
// kicks the user back to the login page
function failLogin($message = 'unspecified')
{
	session::redirect('login.php?loginFailed='.(rawurlencode($message)));
	exit();
}


session::sessionStart();


if(!isset($_POST['action']))
	failLogin('no action was sent to the server');	


switch($_POST['action'])
{
	// processing of the HTML login form is done here
	case 'login':
	
		try
		{
			if(!isset($_POST['userName']) || !isset($_POST['password']))
				failLogin('the username or password is not set in POST.');

			if($_POST['userName'] == '')
				failLogin('empty user name');
			
			if($_POST['password'] == '')
				failLogin('empty password');

			// stores the session data in the SESSION superglobal
			$_SESSION['token'] = new session();
				
			// checks for a valid username and password, redirecting the user to Donation Manager page
			// after successfully logging in
			if($_SESSION['token']->userLogin($_POST['userName'], $_POST['password']))
				$_SESSION['token']->sessionRedirect('../donation_manager.php');
			else
			{
				unset($_SESSION['token']);
				failLogin('invalid credentials');
			}
		}
		catch(Exception $exception)
		{
			failLogin('unexpected exception: '.$exception->getMessage());
		}
		break;


	// logging off is handled here
	case 'logoff':
		$_SESSION['token']->userLogoff();
		session::redirect('../login.php');
		break;
	

	default:
		failLogin('unrecognized action: '.$POST['action']);
}

?>
