<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script checks whether the user has a valid session and is logged in. If not, the user is booted back to
 * the login page. Require() this script at the top of pages that require user login to view. Note that if this script
 * is used on a page that is not in the donation tracker root directory, it won't be able to find the login page and the
 * user will get a 404 error if the redirection is triggered.
 * 
 */
	
require_once(dirname(__FILE__) . '/../classes/session.php');
session::sessionStart();

// contains security checks that should be run at the beginning of restricted pages
// makes sure that the viewer has a valid session before displaying a restricted page
// if not, require them to log in and then take them to the page they were trying to load

if(!session::isLoggedIn())
{		
	session::redirect('web_interface_support/login.php');
	exit();
}

?>