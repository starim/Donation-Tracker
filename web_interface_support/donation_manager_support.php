<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script provides the server layer that interacts with the Javascript on the donation manager page through AJAX.
 * 
 */


// HTTP/1.1 header to prevent browser caching
header('Cache-Control: no-store, no-cache, must-revalidate');

require_once(dirname(__FILE__) .'/../classes/session.php');
require_once(dirname(__FILE__).'/../classes/DBConnection.php');
require_once(dirname(__FILE__).'/../classes/Date.php');

session::sessionStart();

// arbitrarily set the time zone to GMT so the PHP date handling functions won't throw errors
if(ini_get('date.timezone') === false)
	date_default_timezone_set('GMT');


// initializes variables
$returnMessage = array();	// stores error messages
$failSecurityCheck = false;	// this will be set to true if a check in the security section fails

/* 
 * 	security section
 * 
 * 	credentials for using this script are checked here
 * 	NOTHING is allowed to run before this security check
 *	 
 */


// ensures that this request originated from a user with a valid session on this web server

if(!isset($_SESSION['token']))
	die(json_encode(array('status' => false, 'message' => 'Your session with the server timed out. Refresh the page to log in again.')));


if(!isset($_POST['IDCheck1']))
{
	$failSecurityCheck = true;
	$returnMessage[] = 'IDCheck1 is not set in POST';
}

if(!isset($_POST['IDCheck2']))
{
	$failSecurityCheck = true;
	$returnMessage[] = 'IDCheck2 is not set in POST';
}

// credentials are checked here
// IDCheck1 is the sessionID
// IDCheck2 is the fingerprint
if(!$failSecurityCheck && ($_POST['IDCheck1'] != $_SESSION['token']->getSessionID()))
{
	$failSecurityCheck = true;
	$returnMessage[] = 'IDCheck1 (session ID) does not match the expected value (given='.$_POST['IDCheck1'].' vs expected='.$_SESSION['token']->getSessionID().')';
}
if(!$failSecurityCheck && ($_POST['IDCheck2'] != $_SESSION['token']->getFingerprint()))
{
	$failSecurityCheck = true;
	$returnMessage[] = 'IDCheck2 (fingerprint) does not match the expected value (given='.$_POST['IDCheck2'].' vs expected='.$_SESSION['token']->getFingerprint().')';
}


// if there is a security problem, exit here without executing any code beyond this point
if($failSecurityCheck)
	die(json_encode(array('status' => false, 'message' => 'Security error: '.(DEBUG_MODE ? implode('; ', $returnMessage) : 'invalid credentials'))));
	
// set the fingerprint
$_GET['fingerprint'] = $_POST['IDCheck2'];

/*
 * 	end security section
 * 
 * 
 */
 

if(!isset($_POST['action']))
	die(json_encode(array('status' => false, 'message' => 'No action was specified when communicating with the server.')));
	
// the action variable determines what this script will do
switch($_POST['action'])
{
	// returns whether or not all expected database tables are present
	case 'isDBSetUp':
		
		try
		{
			$database = new DBConnection();
			$missingTables = $database->donationTrackerDBCheck();
			echo(json_encode(array('status' => true, 'message' => '', 'isDBSetUp' => empty($missingTables))));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		break;



	
	// returns whether or not the donation tracker database is properly set up
	case 'isDBConfigured':
		
		try
		{
			$database = new DBConnection();
			$isDBConfigured = $database->isDonationTrackerDBConfigured();
			echo(json_encode(array('status' => true, 'message' => '', 'isDBConfigured' => $isDBConfigured)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		break;
	
	
	
	
	// returns an array containing the names of all the donation counter blueprints available
	case 'getCounterBlueprintList':
	
		try
		{
			if(!isset($_POST['simpleCountersOnly']))
				throw new Exception('The parameter "simple counters only" was not sent to the server.');
			
			// convert the simpleCountersOnly parameter from the string "true" or "false" to a boolean
			$simpleCountersOnly = ($_POST['simpleCountersOnly'] == 'true');
			
			
			$database = new DBConnection();
			
			$whereClause = '';
			if($simpleCountersOnly)
				$whereClause = ' WHERE ItemsOwedTextElement IS NULL AND ItemsOwedProgressBar IS NULL AND ItemsGivenTextElement IS NULL AND ItemsGivenProgressBar IS NULL AND PriceTextElement IS NULL';
			$result = $database->query("SELECT `Description` FROM ".DBConnection::$counterBlueprintsTable."$whereClause ORDER BY `Description` ASC;");
			
			$blueprintList = array();
			$numBlueprints = mysql_num_rows($result);
			for($i = 0; $i < $numBlueprints; $i++)
				$blueprintList[] = mysql_result($result, $i, 0);
			
			echo(json_encode(array('status' => true, 'message' => '', 'blueprint list' => $blueprintList)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
	
		break;
	
	
	
	
	// generates a new list of item categories to be shown on the donation manager page
	case 'refreshItemCategories':
	
		try
		{
			$database = new DBConnection();
			$result = $database->query('SELECT * FROM '.DBConnection::$itemCategoriesTable);
			
			$itemCategoriesHTML = '';
			
			if(mysql_num_rows($result) > 0)
			{
				while(($itemCategory = mysql_fetch_assoc($result)) !== false)
				{
					$itemCategoriesHTML .= "<div id=\"category_{$itemCategory['ID']}\" onclick=\"selectItemCategory({$itemCategory['ID']})\">
												<p class=\"itemCategoryName\">{$itemCategory['Name']}</p>";
												
					if($itemCategory['PaymentThreshold'] == 0)
						$itemCategoriesHTML .= "<p class=\"itemCategorySubText\">These items are simple donation counters.</p>";
					else
						$itemCategoriesHTML .= "<p class=\"itemCategorySubText\">Price per item: \${$itemCategory['PaymentThreshold']}</p>
												<p class=\"itemCategorySubText\">Max purchases/item: ".($itemCategory['MaxItemsAllowed'] == 0 ? 'unlimited' : $itemCategory['MaxItemsAllowed'])."</p>";
	
					$itemCategoriesHTML .= "</div>";
				}
			}
			else
				$itemCategoriesHTML = '<p>No categories have been set up yet.</p>';
			
			echo(json_encode(array('status' => true, 'message' => '', 'itemCategoriesHTML' => $itemCategoriesHTML)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// creates a new item category in the database
	case 'createCategory':
	
		try
		{
			// check the validity of the passed parameters
			
			if(!isset($_POST['categoryType']) || !isset($_POST['categoryName']) || !isset($_POST['paymentThreshold']) || !isset($_POST['maxItemsAllowed']))
				throw new Exception('Required parameters were not passed to the server when requesting creation of an item category.');
				
			$categoryName = trim($_POST['categoryName']);
			$paymentThreshold = trim($_POST['paymentThreshold']);
			$maxItemsAllowed = trim($_POST['maxItemsAllowed']);
	
			
			switch($_POST['categoryType'])
			{
				case 'limited pages':
				
					if($maxItemsAllowed == '')
						throw new Exception('The max number of copies per item must be filled in for this category type.');
					
					if(!ctype_digit($maxItemsAllowed))
						throw new Exception('The max number of copies per item must be an integer.');
						
				case 'unlimited pages':
				
					if($paymentThreshold == '')
						throw new Exception('The price per item must be filled in for this category type.');
						
					if(!is_numeric($paymentThreshold))
						throw new Exception('The price per item must be a number.');
						
				case 'simple counters':
				
					if($categoryName == '')
						throw new Exception('The category name must be filled in.');
			}
				
	
			// organize and sanitize the passed parameters
			
			$database = new DBConnection();
			
			switch($_POST['categoryType'])
			{
				case 'limited pages':
					$cleanName = $database->sanitize($categoryName);
					$cleanPaymentThreshold = $database->sanitize($paymentThreshold);
					$cleanMaxItemsAllowed = $database->sanitize($maxItemsAllowed);
					break;
					
				case 'unlimited pages':
					$cleanName = $database->sanitize($categoryName);
					$cleanPaymentThreshold = $database->sanitize($paymentThreshold);
					$cleanMaxItemsAllowed = 0;
					break;
					
				case 'simple counters':
					$cleanName = $database->sanitize($categoryName);
					$cleanPaymentThreshold = 0;
					$cleanMaxItemsAllowed = 0;
					break;
					
				default:
					throw new Exception('Unrecognized category type: '.$_POST['categoryType']);
			}

			// insert the new item category
			$database->query("INSERT INTO ".DBConnection::$itemCategoriesTable.
								" SET Name='$cleanName', PaymentThreshold='$cleanPaymentThreshold', MaxItemsAllowed='$cleanMaxItemsAllowed'");
								
			echo(json_encode(array('status' => true, 'message' => '')));
		}
		catch(DBException $exception)
		{
			// show a more friendly error message if the user tried to insert a category with a name that already exists
			if($exception->isMySQLErrorDataAvailable() && ($exception->getMySQLErrorCode() == 1062))
				die(json_encode(array('status' => false, 'message' => 'A category with that name already exists.')));
			else
				die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}

		break;
		
		
		
		
	// removes a category and all associated items from the database
	case 'deleteCategory':
	
		try
		{
			if(!isset($_POST['categoryID']))
				throw new Exception('The category ID was not passed to the server when requesting deletion of an item category.');

			$database = new DBConnection();
			$database->beginTransaction();
			
			$cleanCategoryID = $database->sanitize($_POST['categoryID']);
			
			// removes all items associated with this category and then the category itself
			// tables must be deleted in this order to avoid delete operations failing from foreign key restrictions
			// the foreign keys also prevent all the records from being deleted in a single query
			$database->query("DELETE FROM ".DBConnection::$itemsTable." WHERE CategoryID='$cleanCategoryID'");
			$database->query("DELETE FROM ".DBConnection::$itemCategoriesTable." WHERE ID='$cleanCategoryID'");
			
			$database->commitTransaction();
			
			echo(json_encode(array('status' => true, 'message' => '')));
		}
		catch(Exception $exception)
		{
			$database->rollbackTransaction();
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// generates a new list of items to be displayed on the donation manager page
	case 'refreshItems':
	
		try
		{
			// check for required inputs
			
			if(!isset($_POST['categoryID']))
				throw new Exception('The category ID was not passed to the server when requesting a list of items.');
				
			if(!isset($_POST['showOnlyEnabledItems']))
				throw new Exception('The value for the Show Only Enabled Items control was not passed to the server.');
		

			$database = new DBConnection();
			$cleanItemsTable = DBConnection::$itemsTable;
			$cleanItemCategoriesTable = DBConnection::$itemCategoriesTable;
			$cleanCategoryID = $database->sanitize($_POST['categoryID']);
			
			// a boolean indicating whether this is a simple donation counter category
			// it is tempting to combine this query with the one to retrieve item properties for efficiency, but if there are no
			// items in this category then that query should return no rows, and then it wouldn't be possible to retrieve whether
			// this category is a simple counter category
			$result = $database->query(<<<QUERY
SELECT PaymentThreshold=0.00 AS `Is Simple Counter Category` FROM $cleanItemCategoriesTable WHERE ID='$cleanCategoryID';
QUERY
);
			$isSimpleCounterCategory = (mysql_result($result, 0, 0) != 0);
			

			$showOnlyEnabledItemsClause = ($_POST['showOnlyEnabledItems'] == 'false' ? '' : 'AND I.Active=1');

			// retrieve the list of items
			$result = $database->query(<<<QUERY
SELECT I.ID, I.Active, I.Name, I.Donations, I.ItemsOwed, I.ItemsGiven, IF(I.PriceOverride=0.00, 'Default', I.PriceOverride) AS 'PriceOverride', IC.PaymentThreshold, IC.MaxItemsAllowed
FROM $cleanItemsTable I
INNER JOIN $cleanItemCategoriesTable IC ON IC.ID=I.CategoryID
WHERE CategoryID='$cleanCategoryID' $showOnlyEnabledItemsClause
ORDER BY I.ACTIVE DESC, ID ASC;
QUERY
);

			$itemsData = array();
			while(($item = mysql_fetch_assoc($result)) !== false)
			{
				// convert values that should be booleans from integers to real booleans
				$item['Active'] = ($item['Active'] != 0);
				
				// add a URL for the item's donation counter
				$domainNameString = $_SERVER['SERVER_NAME'];
				// $pathString = strstr($_SERVER['REQUEST_URI'], 'web_interface_support/', true);	only works in PHP 5.3, which not all web hosts use yet
				$position = strpos($_SERVER['REQUEST_URI'], 'web_interface_support/');
				$pathString = substr($_SERVER['REQUEST_URI'], 0, $position);
				$item['Counter URL template'] = 'http://'.$domainNameString.$pathString.'counter_generator.php?item=%itemName%';
				
				// remove the item ID from the properties list to prevent that property from getting a column in the table displayed to the user
				$itemID = $item['ID'];
				unset($item['ID']);
			
				$itemsData[$itemID] = $item;
			}
			
			echo(json_encode(array('status' => true, 'message' => '', 'items' => $itemsData, 'is simple counter category' => $isSimpleCounterCategory)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// creates a new item in the database
	case 'createItem':
	
		try
		{
			if(!isset($_POST['name']) || !isset($_POST['categoryID']) || !isset($_POST['counterBlueprint']))
				throw new Exception('One or more required parameters was not sent to the server.');
				
			$database = new DBConnection();
			
			// sanitizes input variables
			$cleanCategoryID = $database->sanitize(trim($_POST['categoryID']));
			$cleanName = $database->sanitize(trim($_POST['name']));
			$cleanBlueprintName = $database->sanitize($_POST['counterBlueprint']);
			
			// checks for blank inputs
			if($cleanName == '')
				throw new Exception('You must specify the name of this item.');
				
			// checks whether this item has a duplicate name (compared against other items in this category; items in different categories can have the same name)
			$result = $database->query("SELECT I.Name, IC.Name AS 'CategoryName', I.Active FROM ".DBConnection::$itemsTable." I ".
										" LEFT JOIN ".DBConnection::$itemCategoriesTable." IC ON IC.ID=I.CategoryID".
										" WHERE (I.Name='$cleanName' AND I.CategoryID='$cleanCategoryID');");
			
			if(mysql_num_rows($result) != 0)
			{
				$collidingItem = mysql_fetch_assoc($result);
				if($collidingItem['Name'] == trim($_POST['name']))
					throw new Exception(($collidingItem['Active'] ? "An" : "A disabled" )." item named \"{$collidingItem['Name']}\" in the {$collidingItem['CategoryName']} category already has this name. This name must be unique for each item in order for donations to be processed correctly.");
				else
					throw new Exception(($collidingItem['Active'] ? "An" : "A disabled" )." item in the {$collidingItem['CategoryName']} category already has this name. Multiple items in the same category cannot have the same name.");
			}
				
			// inserts the new item into the database
			$cleanItemsTable = DBConnection::$itemsTable;
			$cleanBlueprintsTable = DBConnection::$counterBlueprintsTable;
			$database->query(<<<QUERY
INSERT INTO $cleanItemsTable
(
	`CategoryID`,
	`Name`,
	`CounterBlueprint`)
VALUES
(
	'$cleanCategoryID',
	'$cleanName',
	IF('$cleanBlueprintName'='',
		NULL,
		(SELECT `Index` FROM $cleanBlueprintsTable WHERE `Description`='$cleanBlueprintName'
		UNION
		SELECT NULL
		LIMIT 1)
	)
);
QUERY
);
			
			echo(json_encode(array('status' => true, 'message' => '')));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
				
	// changes the specified property of an item in the database
	case 'changeItemProperty':
		
		try
		{
			if(!isset($_POST['itemID']) || !isset($_POST['propertyName']) || !isset($_POST['newValue']))
				throw new Exception('One or more required parameters was not sent to the server.');
				
			$database = new DBConnection();
			
			$cleanItemID = $database->sanitize($_POST['itemID']);
			$cleanPropertyName = $database->sanitize($_POST['propertyName']);
			$cleanValue = $database->sanitize($_POST['newValue']);
			

			// performs special validity checking where needed for each property type
			// due to its unique requirements, the Donations column update also happens here
			switch($_POST['propertyName'])
			{
				case 'Donations':
				
					// if the donations are adjusted then the Items Owed and Items Given stats need to be recalculated
				
					// get data on this item
					$result = $database->query("SELECT IF(I.PriceOverride=0.00, IC.PaymentThreshold, I.PriceOverride) AS 'PaymentThreshold', IC.MaxItemsAllowed, I.Name, I.ItemsOwed, I.ItemsGiven
												FROM `".DBConnection::$donationTrackerSchema."`.`Item_Categories` IC
													INNER JOIN `".DBConnection::$donationTrackerSchema."`.`Items` I ON I.CategoryID=IC.ID
												WHERE I.ID='$cleanItemID' LIMIT 1");
					$itemData = mysql_fetch_assoc($result);
													
					$currentAmount = number_format($_POST['newValue'], 2, '.', '');
					
					// calculate the new number of items owed and the new donation amount
					// if the payment threshold is 0 then this item functions simply as a counter--it can never have items owed
					if($itemData['PaymentThreshold'] != 0)
					{
						$newItemsOwed = floor($currentAmount/$itemData['PaymentThreshold']);
						$newAmount = $currentAmount - ($newItemsOwed * $itemData['PaymentThreshold']);
					}
					else
					{
						$newItemsOwed = 0;
						$newAmount = $currentAmount;
					}
					
					// check whether this donation has put the item over its allowed maximum number of purchases
					if(($itemData['MaxItemsAllowed'] != 0) && ($itemData['ItemsOwed'] + $itemData['ItemsGiven'] + $newItemsOwed + $newAmount/$itemData['PaymentThreshold']) > $itemData['MaxItemsAllowed'])
					{
						// store the amount by which this donation exceeds the maximum allowed and log this in the errors table
						// this excess money will have to handled by the seller
						$excessAmount = '$'.number_format(($itemData['ItemsOwed'] + $itemData['ItemsGiven'] + $newItemsOwed - $itemData['MaxItemsAllowed']) * $itemData['PaymentThreshold'] + $newAmount, 2);
		
						throw new Exception("Cannot update the donation amount for item {$itemData['Name']}: the specified amount would cause this item to exceed its maximum allowed donations by $excessAmount.");
					}
					
					// update the item's data in the database
					if($newItemsOwed != 0)
					{
						// if the number of items owed changed, update both the Donations and ItemsOwed columns
						$cleanNewItemsOwed = $database->sanitize($newItemsOwed);
						$cleanNewAmount = $database->sanitize($newAmount);
						$database->query("UPDATE ".DBConnection::$itemsTable." SET Donations='$cleanNewAmount', ItemsOwed=ItemsOwed+'$cleanNewItemsOwed' WHERE ID='$cleanItemID'");
					}
					else
					{
						// if the ItemsOwed counter didn't change, just update the Donations column
						$cleanNewAmount = $database->sanitize($newAmount);
						$database->query("UPDATE ".DBConnection::$itemsTable." SET Donations='$cleanNewAmount' WHERE ID='$cleanItemID'");
					}
	
					// get the new values to display on the page
					$result = $database->query("SELECT Donations, ItemsOwed FROM ".DBConnection::$itemsTable." WHERE ID='$cleanItemID' LIMIT 1");
					if(mysql_num_rows($result) == 0)
						throw new Exception('The database contains no items that match an item ID of '.$_POST['itemID'].'.');
					else
					{
						$newValues = mysql_fetch_assoc($result);
						
						if($itemData['PaymentThreshold'] == 0)
						{
							echo(json_encode(array('status' => true, 'message' => '', 'simpleResponse' => true, 'newValue' => $newValues['Donations'])));
						}
						else
						{
							// creates the array that will be returned with the new values
							// the index is the index of the column in which each value will be displayed in the items table
							$newValues = array(1 => $newValues['Donations'], 2 => $newValues['ItemsOwed']);
							
							echo(json_encode(array('status' => true, 'message' => '', 'simpleResponse' => false, 'newValue' => $newValues)));
						}
					}
					
					return;
					
				case 'PriceOverride':
				
					// get data on this item
					$result = $database->query("SELECT IC.PaymentThreshold, IC.MaxItemsAllowed, I.Name, I.ItemsOwed, I.ItemsGiven, I.Donations
												FROM ".DBConnection::$itemCategoriesTable." IC
													LEFT JOIN ".DBConnection::$itemsTable." I ON I.CategoryID=IC.ID
												WHERE I.ID='$cleanItemID' LIMIT 1");
					$itemData = mysql_fetch_assoc($result);

					
					// this column is meaningless for simple counters and should never be updated
					if($itemData['PaymentThreshold'] == 0)
						throw new Exception("Simple counters cannot have a Price/Page property.");
						
					// determine the new payment threshold for this item, and any changes it causes to the ItemsOwed or Donations columns
					if($_POST['newValue'] == 0)
						$newPriceOverride = $itemData['PaymentThreshold'];
					else
						$newPriceOverride = number_format($_POST['newValue'], 2, '.', '');
						
					$newAmount = fmod($itemData['Donations'], $newPriceOverride);
					$additionalItemsOwed = floor($itemData['Donations'] / $newPriceOverride);
					
					// ensure that the new price per page does not cause this item to exceed its allowed number of purchases
					if(($itemData['MaxItemsAllowed'] != 0) && (($additionalItemsOwed + $itemData['ItemsOwed'] + $itemData['ItemsGiven']) > $itemData['MaxItemsAllowed']))
						throw new Exception("Setting the Price/Page property for item {$itemData['Name']} to $$newPriceOverride would cause the item to exceed its allowed number of purchases.");
						
					// insert the new values into the database
						
					$cleanNewPriceOverride = (($_POST['newValue'] == 0) ? '0.00' : $database->sanitize($newPriceOverride));
					$cleanAdditionalItemsOwed = $database->sanitize($additionalItemsOwed);
					$cleanNewAmount = $database->sanitize($newAmount);
						
					$database->query("UPDATE ".DBConnection::$itemsTable."
										SET PriceOverride=$cleanNewPriceOverride, ItemsOwed=ItemsOwed+$cleanAdditionalItemsOwed, Donations=$cleanNewAmount
										WHERE ID='$cleanItemID' LIMIT 1");
										
					// get the new values to display on the page
					$result = $database->query("SELECT Donations, ItemsOwed, PriceOverride FROM ".DBConnection::$itemsTable." WHERE ID='$cleanItemID' LIMIT 1");
					if(mysql_num_rows($result) == 0)
						throw new Exception('The database contains no items that match an item ID of '.$_POST['itemID'].'.');
					else
					{
						$newValues = mysql_fetch_assoc($result);
						
						// a Price Override of 0 displays as 'Default' to the user
						if($newValues['PriceOverride'] == 0)
							$newValues['PriceOverride'] = 'Default';

						// creates the array that will be returned with the new values
						// the index is the index of the column in which each value will be displayed in the items table
						$newValues = array(1 => $newValues['Donations'], 2 => $newValues['ItemsOwed'], 4 => $newValues['PriceOverride']);
						
						echo(json_encode(array('status' => true, 'message' => '', 'simpleResponse' => false, 'newValue' => $newValues)));
					}
					
					return;
					
				case 'ItemsOwed':
				case 'ItemsGiven':
				
					// prevent the user from setting the items owed to be greater than the maximum purchaseable number for limited items
					
					// get data on this item
					$result = $database->query("SELECT IC.PaymentThreshold, IC.MaxItemsAllowed, I.ItemsOwed, I.ItemsGiven, I.Name
												FROM `".DBConnection::$donationTrackerSchema."`.`Item_Categories` IC
													LEFT JOIN `".DBConnection::$donationTrackerSchema."`.`Items` I ON I.CategoryID=IC.ID
												WHERE I.ID='$cleanItemID' LIMIT 1");

					$itemData = mysql_fetch_assoc($result);
					
					// these columns are meaningless for simple counters and should never be updated
					if($itemData['PaymentThreshold'] == 0)
						throw new Exception("Simple counters cannot have an {$_POST['propertyName']} property.");
					
					// this checking is irrelevant for unlimited items
					if($itemData['MaxItemsAllowed'] == 0)
						break;

					if((($_POST['propertyName'] == 'ItemsOwed') && (($_POST['newValue'] + $itemData['ItemsGiven']) > $itemData['MaxItemsAllowed'])) ||
						(($_POST['propertyName'] == 'ItemsGiven') && (($_POST['newValue'] + $itemData['ItemsOwed']) > $itemData['MaxItemsAllowed'])))
					{
						throw new Exception("Setting the {$_POST['propertyName']} property for item {$itemData['Name']} to {$_POST['newValue']} would cause the item to exceed its allowed number of purchases.");
					}
						
					break;
				
					
				case 'Name':
				
					// check whether the specified new item name will be unique
					$result = $database->query("SELECT
											I.`Active`,
											IF(IC.`Name`=(SELECT IC_ThisItem.`Name` FROM ".DBConnection::$itemsTable." I_ThisItem INNER JOIN ".DBConnection::$itemCategoriesTable." IC_ThisItem ON IC_ThisItem.`ID`=I_ThisItem.`CategoryID` WHERE I_ThisItem.`ID`='$cleanItemID' LIMIT 1), NULL, IC.`Name`) AS `CategoryName`
										FROM ".DBConnection::$itemsTable." I
										INNER JOIN ".DBConnection::$itemCategoriesTable." IC ON IC.`ID`=I.`CategoryID`
										INNER JOIN ".DBConnection::$itemCategoriesTable." IC_ThisItem ON IC_ThisItem.`ID`=I.`CategoryID`
										WHERE I.`ID`<>'$cleanItemID' AND I.`Name`='$cleanValue'
										LIMIT 1;");
										
					if(mysql_num_rows($result) != 0)
					{
						$conflictingItem = mysql_fetch_assoc($result);
						throw new Exception(($conflictingItem['Active'] != 0 ? "An" : "A disabled" ).
							" item in ".
							(is_null($conflictingItem['CategoryName']) ? 'this category' : "the {$conflictingItem['CategoryName']} category").
							" already has the same name. Multiple items in the same category cannot have the same name.");
					}
						
					break;
			}
			
			
			// insert the new value into the database
			try
			{
				$database->query("UPDATE ".DBConnection::$itemsTable." SET `$cleanPropertyName`='$cleanValue' WHERE ID='$cleanItemID' LIMIT 1");
			}
			catch(DBException $exception)
			{
				// display a more friendly error message if the user tried to set a column with a UNIQUE index to a value that another item already has
				if($exception->getMySQLErrorCode() == 1062)
					throw new Exception("An existing item already has \"{$_POST['newValue']}\" as its {$_POST['propertyName']} value. This property must be unique across all items.");
				else
					throw $exception;
			}
			
			// let the database format the new value, then retrieve and return it
			$result = $database->query("SELECT `$cleanPropertyName` FROM ".DBConnection::$itemsTable." WHERE ID='$cleanItemID' LIMIT 1");
			if(mysql_num_rows($result) == 0)
				throw new Exception('The database contains no items that match an item ID of '.$_POST['itemID'].'.');
			else
			{
				$newValue = mysql_result($result, 0, 0);
				
				// a PriceOverride field value of 0 displays as the string 'Default'
				if($_POST['propertyName'] == 'PriceOverride' && $newValue == 0)
					$newValue = 'Default';
				
				echo(json_encode(array('status' => true, 'message' => '', 'simpleResponse' => true, 'newValue' => $newValue)));
			}
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// returns the current settings for the Donation Manager for display in the program options popup window
	case 'loadProgramOptions':
	
		try
		{
			$database = new DBConnection(true);
			$optionsMap = $database->getAllOptionsValues();
			echo(json_encode(array('status' => true, 'message' => '', 'optionsMap' => $optionsMap)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
	
		break;
		
		
		
		
	// changes Donation Manager options
	case 'changeProgramOptions':
	
		try
		{
			$database = new DBConnection(true);
			
			// sanitize variables for insertion into the options table
			if(!isset($_POST['options']))
				throw new Exception('No options were passed to the server to set.');
			
			$database = new DBConnection(true);
			$database->setOptionsValues($_POST['options']);

			echo(json_encode(array('status' => true, 'message' => '')));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
	
		break;	
				
		
		
		
	// determines whether any IPNs are in the IPN Records table
	case 'checkIPNsExist':
	
		try
		{
			$database = new DBConnection(true);
			$result = $database->query("SELECT `Index` FROM ".DBConnection::$IPNRecordTable);
			
			echo(json_encode(array('status' => true, 'message' => '', 'IPNsExist' => (mysql_num_rows($result) != 0))));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
	
		break;
				
		
		
		
	// returns a list of IPNs to display to the user
	case 'refreshIPNList':
		
		try
		{
			$database = new DBConnection(true);
			$result = $database->query("SELECT R.Index, R.Amount, R.TransactionType, R.OriginalText, R.Timestamp, I.Name AS 'ItemName', IC.Name AS 'CategoryName' FROM ".DBConnection::$IPNRecordTable." R
										LEFT JOIN ".DBConnection::$itemsTable." I ON I.ID=R.ItemID
										LEFT JOIN ".DBConnection::$itemCategoriesTable." IC ON IC.ID=I.CategoryID");
			
			$numIPNs = mysql_num_rows($result);
			
			$IPNList = array();
			while(($IPNData = mysql_fetch_assoc($result)) !== false)
			{
				$IPNIndex = $IPNData['Index'];
				unset($IPNData['Index']);;
				
				// santizes the elements for display in a web page
				foreach($IPNData as $index => $element)
				{
					if($index == 'Timestamp')
					{
						$timestamp = new Date($element);
						$IPNData[$index] = htmlentities($timestamp->getStandardUSDateTime(true, true, false), ENT_NOQUOTES, 'UTF-8');
					}
					else
						$IPNData[$index] = htmlentities($element, ENT_NOQUOTES, 'UTF-8');
						
					$IPNData[$index] = str_replace("\n", '<br />', $IPNData[$index]);
				}
				
				// adds the newly prepared IPN to the list of IPNs
				$IPNList[$IPNIndex] = $IPNData;
			}
			
			echo(json_encode(array('status' => true, 'message' => '', 'numIPNs' => $numIPNs, 'IPNList' => $IPNList)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// returns whether there are any errors in the Errors table
	case 'checkErrorsExist':
	
		try
		{
			$database = new DBConnection(true);
			$result = $database->query("SELECT `Index` FROM ".DBConnection::$errorsTable);
			
			echo(json_encode(array('status' => true, 'message' => '', 'errorsExist' => (mysql_num_rows($result) != 0))));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
	
		break;
		
		
		
		
	// retrieves a list of IPN processor errors to display to the user
	case 'refreshErrorList':
		
		try
		{
			$database = new DBConnection(true);
			$result = $database->query("SELECT * FROM ".DBConnection::$errorsTable);
			
			$numErrors = mysql_num_rows($result);
			
			$errorList = array();
			while(($errorData = mysql_fetch_assoc($result)) !== false)
			{
				$errorIndex = $errorData['Index'];
				unset($errorData['Index']);
				
				// santizes the elements for display in a web page
				foreach($errorData as $index => $element)
				{
					if($index == 'Timestamp')
					{
						$timestamp = new Date($element);
						$errorData[$index] = htmlentities($timestamp->getStandardUSDateTime(true, true, false), ENT_NOQUOTES, 'UTF-8');
					} 
					else
						$errorData[$index] = htmlentities($element, ENT_NOQUOTES, 'UTF-8');
						
					$errorData[$index] = str_replace("\n", '<br />', $errorData[$index]);
				}
				
				// adds the newly prepared error to the list of errors
				$errorList[$errorIndex] = $errorData;
			}
			
			echo(json_encode(array('status' => true, 'message' => '', 'numErrors' => $numErrors, 'errorList' => $errorList)));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}
		
		break;
		
		
		
		
	// erases an error in the Errors table from the database
	case 'deleteError':
	
		try
		{
			if(is_null($_POST['errorIndex']))
				throw new Exception('No error index was sent to the server.');
			
			$database = new DBConnection(true);
			$cleanIndex = $database->sanitize($_POST['errorIndex']);
			$database->query("DELETE FROM ".DBConnection::$errorsTable." WHERE `Index`='$cleanIndex'");
			
			echo(json_encode(array('status' => true, 'message' => '')));
		}
		catch(Exception $exception)
		{
			die(json_encode(array('status' => false, 'message' => $exception->getMessage())));
		}

		break;
		
		
		
		
	default:
		die(json_encode(array('status' => false, 'message' => 'Error in '.basename(__FILE__).': Unknown action specified ("'.$_POST['action'].'")')));
}

?>
