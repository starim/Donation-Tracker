<?php

/**
 * @author: Brent Houghton and Dustin Kilpatrick, © 2009
 * 
 * This script contains the login page displayed to users when they try to access the donation manager.
 * 
 */


require_once(dirname(__FILE__).'/../classes/session.php');

function buildGETParametersString()
{
	$parameters = '';
	$first = true;
	foreach($_GET as $name => $value)
	{
		if($first)
			$first = false;
		else
			$parameters .= '&';
			
		$parameters .= urlencode($name).'='.urlencode($value);
	}
	
	return $parameters;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Donation Manager</title>
	<meta charset="utf-8" />
	<!-- activates Chrome Frame for compatibility with older Internet Explorer browsers -->
	<!--[if lte IE 9]>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<![endif]-->
	<!-- for IE users that don't have Chrome Frame, try to support them with this HTML 5 shim -->
	<!--[if lte IE 8]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- <link rel="shortcut icon" type="image/x-icon" href="" /> -->
	<meta name="copyright" content="© 2009 Brent Houghton">
	
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<link rel="stylesheet" type="text/css" href="css/login_styles.css" />
	
	<script type="text/javascript" src="javascript/jquery.js"></script>
	<script type="text/javascript">
	
		var DEBUG_MODE = <?php if(DEBUG_MODE) {echo 'true'; } else { echo 'false'; } ?>;
		
	</script>
</head>

<body>

<!-- urges users of IE 7 and below to upgrade their browser - code copied from The Internet Explorer 6 Countdown http://ie6countdown.com/ -->
<!--[if lte IE 7]> <div style='clear: both; height: 59px; text-align: center; margin-bottom: 0px; height: 42px;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->

<div id="pageContent">

	<img id="headerImage" src="images/donation_manager_heading.png" alt="" />
	
	<p id="loginFailedMessage" class="<?php if(isset($_GET['loginFailed'])) echo 'visible'; ?>">The given user name/password combination is invalid, or the MySQL server could not be found.</p>
	
	<form action="login_support.php<?php $parameters = buildGETParametersString(); if($parameters != '') echo "?$parameters"; ?>" method="post">
		<fieldset>
			<legend>Login information</legend>
			
			<input type="hidden" name="action" value="login" />
			
			<div class="loginFieldArea">
				<span class="inlineInputGroup">
					<label for="userName">User Name:</label><input class="loginInput" type="text" name="userName" id="userName" />
				</span>
				<span class="inlineInputGroup">
					<label for="password">Password:</label><input class="loginInput" type="password" name="password" id="password" maxlength="40" />
				</span>
			</div>
		</fieldset>
		<input class="formSubmitButton" type="submit" value="Login" />
	</form>
</div>
<?php include('footer.php'); ?>
<script type="text/javascript">
	document.getElementById('userName').focus();
</script>
</body>
</html>
