<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This script defines important configurable parameters for the Donation
 * Manager.
 * 
 */


// The username and password for the MySQL account the Donation Manager will
// use for automated operations like processing IPNs and generating donation
// counter images.
define('DB_USER', 'USERNAMEGOESHERE');
define('DB_PASSWORD', 'PASSWORDGOESHERE');

// The domain name or IP address and the port to connect to in order to access
// the MySQL server. If MYSQL_SERVER is set to an empty string, the default
// MySQL server set in php.ini will be used instead.
define('DB_SERVER', 'SERVERGOESHERE');
define('DB_PORT', 'PORTGOESHERE');

// The database schema which the Donation Manager will use.
define('DB_SCHEMA', 'DATABASENAMEGOESHERE');

// This defines the URL that will be used by the link to the source code shown
// in the footer of all Donation Manager web pages.
// Note that if you make changes or extentions to this system, it is a LEGAL
// REQUIREMENT that you provide a link to download the modified code. To comply
// with this requirement, simply host your code online and change the URL here
// to point to your hosted copy of the code.
// See LICENSE.txt for the details of this requirement.
define('SOURCE_CODE_URL', 'http://github.com/starim/Donation-Tracker');

// This defines the text of the copyright notice that is placed in the footer
// of every page of the web interface.
define('COPYRIGHT_NOTICE', '© 2009 Brent Houghton');

// Sets whether debug mode is turned on. When on, some scripts will make
// internal information available to external callers to allow easy debugging.
// This option should ALWAYS be turned off on production systems!
define('DEBUG_MODE', false);

?>
