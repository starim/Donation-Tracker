<?php

/**
 * @author: Brent Houghton, © 2009
 * 
 * This class provides an interface that scripts outside the Donation Manager can use to get information on merchandise tracked by the Donation Manager.
 * Note that this is a read-only interface; there is no scriptable way to manipulate the Donation Manager, only get the status of merchandise.
 *
 *
 * The following item properties are returned by the getItemData() method:
 *
 * Category (string): the name of the category to which this item belongs
 * Donation Toward Next (floating point number): the dollar amount donated toward buying the next copy of this item
 * Total Donations (floating point number): the total dollar amount donated for this item
 * Copies Owed (integer): the number of copies owed for this item
 * Copies Done (integer): the number of copies given out for this item
 * Price Per Copy (floating point number): the dollar price for each copy of this item
 * Max Copies Available (integer or null): the maximum number of copies available for limited purchase items, or null for unlimited purchase items
 * Donation Goal Reached (boolean or null): whether the item has reached its maximum donation amount for limited purchase items, or null for unlimited purchase items
 * Is Active (boolean): whether this item is marked as active (in the Donation Manager items can be deactivated so that they stop accepting payments, which is useful for discontinued items)
 *
 *
 * Sample usage:
 *

<?php

require_once('<path to Donation Manager>/public_interface/MerchandiseStatusInterface.php');

try
{
	// initialize the interface
	$itemData = new MerchandiseStatusInterface();
}
catch(Exception $exception)
{
	// handle the Donation Manager failing to initialize
}

// get a list of the items tracked by the Donation Manager
$itemsList = $itemData->getItemList();

// get all the properties for an item named "Item #1"
$item1Properties = $itemData->getItemData('Item #1');
if(is_null($item1Properties))
	echo 'Error: no item named "Item #1" exists in the Donation Manager.'."\n";
else
	echo 'Item #1 properties: '.print_r($item1Properties, true)."\n";

// display the progress of "Item #2" toward its donation goal
$item2Properties = $itemData->getItemData('Item #2');
if(is_null($item2Properties))
	echo 'Error: no item named "Item #2" exists in the Donation Manager.';
else if(is_null($item2Properties['Donation Goal Reached']))
	echo 'Item #2 has an unlimited number of copies available so it does not have a donation goal.';
else
{
	$donationGoal = '$'.number_format($item2Properties['Max Copies Available'] * $item2Properties['Price Per Copy'], 2);
	
	if($item2Properties['Donation Goal Reached'])
	{
		echo "Item #2 has reached its donation goal of $donationGoal!\n";
	}
	else
		echo "Item #2 donation progress: $".$item2Properties['Total Donations']." / $donationGoal\n";
}

?>

 * 
 */


require_once(dirname(__FILE__).'/../configuration.php');
require_once(dirname(__FILE__).'/../classes/DBConnection.php');


class MerchandiseStatusInterface
{
	// this array stores data on every item known to the Donation Manager, indexed by item name
	private $itemData;


	// Constructor
	public function MerchandiseStatusInterface()
	{
		try
		{
			$database = new DBConnection(false);
			$result = $database->query(
				"SELECT
					I.`Name`,
					IC.`Name` AS `Category`,
					I.`Donations` AS `Donations Toward Next`,
					(((I.`ItemsOwed` + I.`ItemsOwed`) * IF(I.`PriceOverride`=0.00, IC.`PaymentThreshold` , I.`PriceOverride`)) + I.`Donations`) AS `Total Donations`,
					I.`ItemsOwed` AS `Copies Owed`,
					I.`ItemsGiven` AS `Copies Done`,
					IF(I.`PriceOverride`=0.00, IC.`PaymentThreshold` , I.`PriceOverride`) AS `Price Per Copy`,
					IF(IC.`MaxItemsAllowed`=0, NULL, IC.`MaxItemsAllowed`) AS `Max Copies Available`,
					IF(IC.`MaxItemsAllowed`=0, NULL, (I.`ItemsOwed` + I.`ItemsGiven`)>=IC.`MaxItemsAllowed`) AS `Donation Goal Reached`,
					I.`Active`<>0 AS `Is Active`
				FROM `".DBConnection::$donationTrackerSchema."`.`Items` I
				INNER JOIN `".DBConnection::$donationTrackerSchema."`.`Item_Categories` IC ON IC.`ID`=I.`CategoryID`;"
			);

			// set up the array that will store all the item data
			$this->itemData = array();
			while(($itemRow = mysql_fetch_assoc($result)) !== false)
			{
				$itemData = array();
				
				$itemName = $itemRow['Name'];
				unset($itemRow['Name']);
				
				// convert columns that are supposed to store boolean values to proper boolean values, since MySQL can't handle real booleans
				if(!is_null($itemRow['Donation Goal Reached']))
					$itemRow['Donation Goal Reached'] = ($itemRow['Donation Goal Reached'] != 0);
				$itemRow['Is Active'] = ($itemRow['Is Active'] != 0);
	
				foreach($itemRow as $index => $value)
					$itemData[$index] = $value;
	
				$this->itemData[$itemName] = $itemData;
			}
		}
		catch(Exception $exception)
		{
			throw new Exception('Error retrieving information on Donation Manager merchandise: '.$exception->getMessage());
		}
	}
	
	// returns an array containing all the names for items known to the Donation Manager system
	public function getItemList()
	{
		return array_keys($this->itemData);
	}


	// returns an array of properties for the specified item (or all items if no item is specified)
	//
	// parameters:
	//		$itemName: an optional parameter giving the name of the item whose data should be returned
	//					if not specified, an array will be returned with data for all items, indexed by item name
	//
	// returns:
	//		if a specified item name was specified, returns an associative array with the following properties or
	//		null if no item with the given name exists
	//		item properties:
	//			Donations Toward Next: the dollar amount donated toward buying the next copy of this item
	//			Copies Owed: the number of copies owed for this item
	//			Copies Done: the number of copies given out for this item
	//
	//		if no item name was specified, returns an associative array of data for every item, indexed by item name
	public function getItemData($itemName = null)
	{
		if(is_null($itemName))
			return $this->itemData;
		else
		{
			if(isset($this->itemData[$itemName]))
				return $this->itemData[$itemName];
			else
				return null;
		}
	}
}

?>
