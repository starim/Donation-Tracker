-- This SQL script contains the database structure used in this version of the Donation Manager system.
-- It is used by the setup script to build a fresh copy of the Donation Manager's database entities.
--


CREATE TABLE IF NOT EXISTS `Donation_Counters_Text_Elements` (
	`Index` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`Description` varchar(255) NOT NULL DEFAULT '',
	`FontFileName` varchar(255) NOT NULL,
	`FontSize` int(10) unsigned NOT NULL,
	`Justification` enum('left','center','right') NOT NULL default 'left' COMMENT 'Sets whether the given x-coordinate for this text element should be at the left edge, center, or right edge of the placed element.',
	`ColorRGB` mediumint(8) unsigned zerofill NOT NULL DEFAULT '00000000' COMMENT 'The color to use for this text element. The highest byte gives the red component, the next byte gives the green component, and the lowest byte gives the blue component.',
	`MaxColorRGB` mediumint(8) unsigned zerofill DEFAULT NULL COMMENT 'The color to use when a limited purchase item is maxed out. Structured identically to the ColorRGB column.',
	`XOffset` int(10) unsigned NOT NULL COMMENT 'The distance from the edge specified by XOffsetFrom to the x-coordinate of the top left corner of the text element.',
	`XOffsetFrom` enum('left','right') NOT NULL DEFAULT 'left',
	`YOffset` int(10) unsigned NOT NULL COMMENT 'The distance from the edge specified by YOffsetFrom to the y-coordinate of the top left corner of the text element.',
	`YOffsetFrom` enum('top','bottom') NOT NULL DEFAULT 'top',
	PRIMARY KEY (`Index`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Donation_Counters_Progress_Bars` (
	`Index` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`Description` varchar(255) NOT NULL DEFAULT '',
	`Mode` enum('stretch','duplicate') NOT NULL COMMENT 'Determines whether the progress bar image should be stretched or duplicated to reach the length of the progress bar.',
	`ImageFileName` varchar(255) NOT NULL,
	`MaxImageFileName` varchar(255) DEFAULT NULL COMMENT 'This is the image file to use when the referenced item has reached its maximum allotted purchases.',
	`ExpansionDirection` enum('left','right','up','down') NOT NULL DEFAULT 'right',
	`MaxWidth` int(10) unsigned DEFAULT NULL,
	`XPosition` int(10) unsigned NOT NULL,
	`XOffsetFrom` enum('left','right') NOT NULL DEFAULT 'left',
	`YPosition` int(10) unsigned NOT NULL,
	`YOffsetFrom` enum('top','bottom') NOT NULL DEFAULT 'top',
	PRIMARY KEY (`Index`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Donation_Counters_Blueprints` (
	`Index` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`Description` varchar(255) NOT NULL,
	`BaseImageFileName` varchar(255) NOT NULL,
	`NameTextElement` int(10) unsigned DEFAULT NULL,
	`DonationsTextElement` int(10) unsigned DEFAULT NULL,
	`DonationsProgressBar` int(10) unsigned DEFAULT NULL,
	`ItemsOwedTextElement` int(10) unsigned DEFAULT NULL,
	`ItemsOwedProgressBar` int(10) unsigned DEFAULT NULL,
	`ItemsGivenTextElement` int(10) unsigned DEFAULT NULL,
	`ItemsGivenProgressBar` int(10) unsigned DEFAULT NULL,
	`PriceTextElement` int(10) unsigned DEFAULT NULL,
	PRIMARY KEY (`Index`),
	KEY `FK_Name_Text_Element` (`NameTextElement`),
	KEY `FK_Donations_Text_Element` (`DonationsTextElement`),
	KEY `FK_Donations_Progress_Bar` (`DonationsProgressBar`),
	KEY `FK_Items_Owed_Text_Element` (`ItemsOwedTextElement`),
	KEY `FK_Items_Owed_Progress_Bar` (`ItemsOwedProgressBar`),
	KEY `FK_Items_Given_Text_Element` (`ItemsGivenTextElement`),
	KEY `FK_Items_Given_Progress_Bar` (`ItemsGivenProgressBar`),
	KEY `FK_Price_Text_Element` (`PriceTextElement`),
	CONSTRAINT `FK_Donations_Progress_Bar` FOREIGN KEY (`DonationsProgressBar`) REFERENCES `Donation_Counters_Progress_Bars` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Donations_Text_Element` FOREIGN KEY (`DonationsTextElement`) REFERENCES `Donation_Counters_Text_Elements` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Items_Given_Progress_Bar` FOREIGN KEY (`ItemsGivenProgressBar`) REFERENCES `Donation_Counters_Progress_Bars` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Items_Given_Text_Element` FOREIGN KEY (`ItemsGivenTextElement`) REFERENCES `Donation_Counters_Text_Elements` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Items_Owed_Progress_Bar` FOREIGN KEY (`ItemsOwedProgressBar`) REFERENCES `Donation_Counters_Progress_Bars` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Items_Owed_Text_Element` FOREIGN KEY (`ItemsOwedTextElement`) REFERENCES `Donation_Counters_Text_Elements` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Name_Text_Element` FOREIGN KEY (`NameTextElement`) REFERENCES `Donation_Counters_Text_Elements` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_Price_Text_Element` FOREIGN KEY (`PriceTextElement`) REFERENCES `Donation_Counters_Text_Elements` (`Index`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Item_Categories` (
	`ID` int(10) unsigned NOT NULL auto_increment,
	`Name` varchar(255) NOT NULL,
	`PaymentThreshold` decimal(11,2) NOT NULL COMMENT 'The amount of money that must be received to earn a new instance of an item (for comics this would be the amount of money needed for a new page of the comic)',
	`MaxItemsAllowed` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The number of instances of each individual item in this category that can be purchased, after which further donations will be considered errors. Set to zero to allow an infinite number of item instances to be purchased.',
	PRIMARY KEY (`ID`),
	CONSTRAINT `UNIQUE_CategoryName` UNIQUE INDEX (`Name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Items` (
	`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`Name` varchar(127) NOT NULL,
	`CategoryID` int(10) unsigned NOT NULL,
	`Donations` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Amount of money donated since the last page owed.',
	`ItemsOwed` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The number of times the donations counter for this item reached the payment threshold set for its category',
	`ItemsGiven` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The number of items of this type that have been given away.',
	`PriceOverride` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'This price overrides the price set by the item category.',
	`CounterBlueprint` int(10) unsigned DEFAULT NULL COMMENT 'The Index number of the donation counter blueprint associated with this item.',
	`URL` varchar(127) NOT NULL DEFAULT '',
	`Active` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'A value of 1 indicates this item can be donated toward, while 0 means that this item is discontinued.',
	PRIMARY KEY (`ID`),
	UNIQUE KEY `Name_UNIQUE` (`Name`),
	KEY `FK_CategoryID` (`CategoryID`),
	KEY `fk_Item_CounterBlueprint` (`CounterBlueprint`),
	CONSTRAINT `fk_Item_CounterBlueprint` FOREIGN KEY (`CounterBlueprint`) REFERENCES `Donation_Counters_Blueprints` (`Index`) ON UPDATE CASCADE,
	CONSTRAINT `FK_CategoryID` FOREIGN KEY (`CategoryID`) REFERENCES `Item_Categories` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `IPN_Record` (
	`Index` int(10) unsigned NOT NULL auto_increment,
	`TransactionID` varchar(255) NOT NULL COMMENT 'The transaction ID assigned by Paypal',
	`Amount` decimal(11,2) unsigned NOT NULL,
	`ItemID` int(10) unsigned NOT NULL COMMENT 'The ID of the item this payment was applied to.',
	`TransactionType` varchar(50) NOT NULL,
	`PayerEmail` varchar(127) NOT NULL COMMENT 'The email address of the donor for this transaction.',
	`OriginalText` text NOT NULL COMMENT 'The original text of the IPN.',
	`Timestamp` datetime NOT NULL COMMENT 'The current time when the IPN was logged.',
	PRIMARY KEY  USING BTREE (`Index`),
	KEY `FK_ItemID` (`ItemID`),
	CONSTRAINT `UNIQUE_TransactionID` UNIQUE INDEX (`TransactionID`),
	CONSTRAINT `FK_ItemID` FOREIGN KEY (`ItemID`) REFERENCES `Items` (`ID`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Errors` (
	`Index` int(10) unsigned NOT NULL auto_increment,
	`ErrorMessage` varchar(255) NOT NULL,
	`IPNText` text,
	`Timestamp` datetime NOT NULL COMMENT 'The time at which this error was logged.',
	PRIMARY KEY  (`Index`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `Options` (
	`Key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`Value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`Key`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Donation_Manager_DB_Current_State`;
CREATE TABLE `Donation_Manager_DB_Current_State` (
	`state-003` VARCHAR(45) NOT NULL COMMENT 'The current state of the Donation Manager database.',
	`date_updated` DATETIME NOT NULL COMMENT 'The last time the database state was updated.'
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;




-- Set up the default counter blueprints.

INSERT INTO `Donation_Counters_Text_Elements`
(`Description`, `FontFileName`, `FontSize`, `Justification`, `ColorRGB`, `MaxColorRGB`, `XOffset`, `XOffsetFrom`, `YOffset`, `YOffsetFrom`)
VALUES
('Name display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 14, 'center', 16777215, NULL, 126, 'left', 26, 'top'),
('Donation amount display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'right', 16777215, 09817134, 113, 'left', 46, 'top'),
('Price display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'left', 16777215, 09817134, 135, 'left', 46, 'top'),
('Donation amount display for the default simple counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'center', 16777215, NULL, 125, 'left', 46, 'top');

INSERT INTO `Donation_Counters_Blueprints`
(
	`Description`,
	`BaseImageFileName`,
	`NameTextElement`,
	`DonationsTextElement`,
	`DonationsProgressBar`,
	`ItemsOwedTextElement`,
	`ItemsOwedProgressBar`,
	`ItemsGivenTextElement`,
	`ItemsGivenProgressBar`,
	`PriceTextElement`
)
VALUES
(
	'Shiny blue donation counter.',
	'blue_counter_shiny.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Price display for the default counter blueprints.' LIMIT 1)
),
(
	'Darker blue donation counter.',
	'blue_counter_darker.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Price display for the default counter blueprints.' LIMIT 1)
),
(
	'Shiny blue simple donation counter.',
	'blue_simple_counter_shiny.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default simple counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
),
(
	'Darker blue simple donation counter.',
	'blue_simple_counter_darker.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default simple counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
);


-- complete the update
INSERT INTO `Donation_Manager_DB_Current_State`
(`state-003`, `date_updated`)
VALUES
('state 3', NOW());

