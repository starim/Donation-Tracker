-- Converts a Donation Tracker database from state 2 to 3.


-- Create a table for tracking the current database state so that the site admin always knows what database updates
-- to apply when upgrading Donation Manager versions.
CREATE TABLE `Donation_Manager_DB_Current_State` (
  `state-003` VARCHAR(45) NOT NULL COMMENT 'The current state of the Donation Manager database.' ,
  `date_updated` DATETIME NOT NULL COMMENT 'The last time the database state was updated.' )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

INSERT INTO `Donation_Manager_DB_Current_State`
(`state-003`, `date_updated`) VALUES
('between state 2 and 3', NOW());


-- Remove the PaypalName column from the items table.
ALTER TABLE `Items`
	DROP COLUMN `PaypalName`,
	DROP INDEX `UNIQUE_PaypalName`;


-- Create a set of default donation counter blueprints that can be used with simple items.

INSERT INTO `Donation_Counters_Text_Elements`
(`Description`, `FontFileName`, `FontSize`, `Justification`, `ColorRGB`, `MaxColorRGB`, `XOffset`, `XOffsetFrom`, `YOffset`, `YOffsetFrom`)
VALUES
('Donation amount display for the default simple counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'center', 16777215, NULL, 125, 'left', 46, 'top');

INSERT INTO `Donation_Counters_Blueprints`
(
	`Description`,
	`BaseImageFileName`,
	`NameTextElement`,
	`DonationsTextElement`,
	`DonationsProgressBar`,
	`ItemsOwedTextElement`,
	`ItemsOwedProgressBar`,
	`ItemsGivenTextElement`,
	`ItemsGivenProgressBar`,
	`PriceTextElement`
)
VALUES
(
	'Shiny blue simple donation counter.',
	'blue_simple_counter_shiny.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default simple counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
),
(
	'Darker blue simple donation counter.',
	'blue_simple_counter_darker.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default simple counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
);


-- Fix a bug from earlier database versions where the price text element on the default donation counter blueprints would have the wrong justification.
UPDATE `Donation_Counters_Text_Elements`
SET `Justification`='left'
WHERE `Description`='Price display for the default counter blueprints.';


-- complete the update
UPDATE `Donation_Manager_DB_Current_State`
SET `state-003`='state 3', `date_updated`=NOW();

