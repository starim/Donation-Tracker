-- Converts a Donation Tracker database from state 1 to 2.


-- Switches all tables to use UTF-8 for text encoding.
ALTER TABLE `Donation_Counters_Blueprints` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Donation_Counters_Progress_Bars` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Donation_Counters_Text_Elements` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Errors` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `IPN_Record` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Item_Categories` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Items` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;
ALTER TABLE `Options` CHARACTER SET=utf8, COLLATE=utf8_unicode_ci;


-- Force item names to be unique.
ALTER TABLE `Items` ADD UNIQUE INDEX `Name_UNIQUE` (`Name` ASC);


-- Increase the size of money amounts that can be stored by two orders of mangitude. Due to the way MySQL
-- stores DECIMAL data, increasing the range of these fields does not require an increase in the size of
-- these fields on disk.
ALTER TABLE `IPN_Record` CHANGE COLUMN `Amount` `Amount` DECIMAL(11,2) UNSIGNED NOT NULL;
ALTER TABLE `Item_Categories` CHANGE COLUMN `PaymentThreshold` `PaymentThreshold` DECIMAL(11,2) NOT NULL COMMENT 'The amount of money that must be received to earn a new instance of an item (for comics this would be the amount of money needed for a new page of the comic)';
ALTER TABLE `Items`
	CHANGE COLUMN `Donations` `Donations` DECIMAL(11,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'Amount of money donated since the last page owed.',
	CHANGE COLUMN `PriceOverride` `PriceOverride` DECIMAL(11,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'This price overrides the price set by the item category.';


-- Convert the Options table from hard-coding the available options into each column, and instead store
-- key-value pairs to allow adding and removing options much more easily in the future.

DROP TEMPORARY TABLE IF EXISTS `ReferralIDTempTable`;
DROP TEMPORARY TABLE IF EXISTS `PaymentCalculationModeTempTable`;

CREATE TEMPORARY TABLE `ReferralIDTempTable` (
  `Value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Value`)
) DEFAULT CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;

CREATE TEMPORARY TABLE `PaymentCalculationModeTempTable` (
  `Value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Value`)
) DEFAULT CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO `ReferralIDTempTable`
(`Value`) VALUES
((SELECT `MY_REFERRAL_ID` FROM `Options` LIMIT 1));

INSERT INTO `PaymentCalculationModeTempTable`
(`Value`) VALUES
((SELECT `PAYMENT_CALCULATION_MODE` FROM `Options` LIMIT 1));

DROP TABLE `Options`;
CREATE TABLE `Options` (
  `Key` VARCHAR(255) NOT NULL,
  `Value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Key`)
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO `Options`
(`Key`, `Value`) VALUES
('MY_REFERRAL_ID', (SELECT `Value` FROM `ReferralIDTempTable` LIMIT 1));
INSERT INTO `Options`
(`Key`, `Value`) VALUES
('PAYMENT_CALCULATION_MODE', (SELECT `Value` FROM `PaymentCalculationModeTempTable` LIMIT 1));


-- Create a set of default donation counter blueprints.

INSERT INTO `Donation_Counters_Text_Elements`
(`Description`, `FontFileName`, `FontSize`, `Justification`, `ColorRGB`, `MaxColorRGB`, `XOffset`, `XOffsetFrom`, `YOffset`, `YOffsetFrom`)
VALUES
('Name display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 14, 'center', 16777215, NULL, 126, 'left', 26, 'top'),
('Donation amount display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'right', 16777215, 09817134, 113, 'left', 46, 'top'),
('Price display for the default counter blueprints.', 'DejaVuSansMono-Bold.ttf', 12, 'left', 16777215, 09817134, 135, 'left', 46, 'top');

INSERT INTO `Donation_Counters_Blueprints`
(
	`Description`,
	`BaseImageFileName`,
	`NameTextElement`,
	`DonationsTextElement`,
	`DonationsProgressBar`,
	`ItemsOwedTextElement`,
	`ItemsOwedProgressBar`,
	`ItemsGivenTextElement`,
	`ItemsGivenProgressBar`,
	`PriceTextElement`
)
VALUES
(
	'Shiny blue donation counter.',
	'blue_counter_shiny.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Price display for the default counter blueprints.' LIMIT 1)
),
(
	'Darker blue donation counter.',
	'blue_counter_darker.png',
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Name display for the default counter blueprints.' LIMIT 1),
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Donation amount display for the default counter blueprints.' LIMIT 1),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(SELECT `Index` FROM `Donation_Counters_Text_Elements` WHERE `Description`='Price display for the default counter blueprints.' LIMIT 1)
);

