<?php

/**
 * @author: Brent Houghton, © 2010
 * 
 * A script for checking whether a new item has been issued and automatically updaing the items owed/given stats.
 * 
 */


define('LOG_FILE', 'autoupdater_log.html');


ob_start();
$startTime = time();
$date = date('F jS, Y at g:i A');
echo "Starting counter update on $date ... <br />";

require_once('configuration.php');
require_once('classes/DBConnection.php');

// gets the list of items with defined URLs
$database = new DBConnection(false);
$result = $database->query("SELECT `ID`, `Name`, `URL`, `ItemsOwed`, `ItemsGiven` FROM ".DBConnection::$itemsTable." WHERE URL<>''");

while(($itemData = mysql_fetch_assoc($result)) !== false)
{
	try
	{
		echo 'Getting the latest page for item "'.htmlentities($itemData['Name'], ENT_NOQUOTES, 'UTF-8').'"...  ';
		
		// set up the connection
		$curl = curl_init($itemData['URL']);
		curl_setopt($curl, CURLOPT_USERAGENT, 'cURL/Donation Tracker Autoupdater');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
		
		// get the HTML page
		$html = curl_exec($curl);
		curl_close($curl);
		if($html === false)
		{
			echo '<span style="color: red">failed</span> (CURL failed to retrieve the page at URL '.htmlentities($itemData['URL'], ENT_NOQUOTES, 'UTF-8').'. The specific error is: '.htmlentities(curl_error($curl), ENT_NOQUOTES, 'UTF-8').')<br />';
			continue;
		}
		$dom = new DOMDocument();
		@$dom->loadHTML($html);
		
		if($dom === false)
		{
			echo '<span style="color: red">failed</span> (could not parse the response as valid HTML--the received response is "'.htmlentities($html, ENT_NOQUOTES, 'UTF-8').'")<br />';
			continue;
		}
		
		// get the the comic number
		/*
			This section requires custom programming. You will have to add a line that sets the comicNumber variable to the value of the latest page for the comic series.
			The sample code here retrieves the comic number from a DOM element with id "comicNumber".
			
		$comicNumber = $dom->getElementById('comicNumber')->textContent;
		*/
		throw new Exception('No rule set for retrieving comic numbers. Add code above line '.__LINE__.' of '.basename(__FILE__).' to fetch the comic number from a comic\'s web page.');
		
		/*
			end of custom programming section
		*/
		
		// check if the attempt to parse a number was successful and if not, give up and move to the next item
		if(!ctype_digit($comicNumber))
		{
			echo '<span style="color: red">failed</span> (retrieved comic number is "'.htmlentities($comicNumber, ENT_NOQUOTES, 'UTF-8').'" (pulled from the title "'.htmlentities($title, ENT_NOQUOTES, 'UTF-8').'") which has been rejected for being non-numeric)<br />';
			continue;
		}
		
		// do not modify the item's properties if the comic number retrieved is less than the number of comics reported in the database
		// if the comic number is equal to the items given then the item's data is already up to date
		if($comicNumber <= $itemData['ItemsGiven'])
		{
			echo '<span style="color: green">success</span> (retrieved comic number is '.htmlentities($comicNumber, ENT_NOQUOTES, 'UTF-8').'; no database update is required for this item)<br />';
			continue;
		}
		
		// if the number is greater than what's in the database, advance the database's items given count and deduct the item owed count
		$pageNumberModification = $comicNumber - $itemData['ItemsGiven'];
		if($pageNumberModification > $itemData['ItemsOwed'])
			$pageNumberModification = $itemData['ItemsOwed'];
		$cleanPageNumberModification = $database->sanitize($pageNumberModification);
		$cleanItemID = $database->sanitize($itemData['ID']);
		$database->query("UPDATE ".DBConnection::$itemsTable." SET `ItemsOwed`=`ItemsOwed`-$cleanPageNumberModification, `ItemsGiven`=`ItemsGiven`+$cleanPageNumberModification WHERE `ID`='$cleanItemID'");
		
		echo '<span style="color: green">success</span> (retrieved comic number is '.htmlentities($comicNumber, ENT_NOQUOTES, 'UTF-8').'; the comic\'s given/owed fields were incremented/deducted by '.htmlentities($pageNumberModification, ENT_NOQUOTES, 'UTF-8').')<br />';
	}
	catch(DBException $exception)
	{
		echo '<span style="color: red">failed</span> (database access error: '.htmlentities($exception->getMessage(), ENT_NOQUOTES, 'UTF-8').'<br />';
	}
}

echo '<br />Complete. Time taken to run script: '.(time() - $startTime).' s';

$logText = ob_get_clean();
if(is_null(LOG_FILE) || (LOG_FILE == ''))
	$logFile = false;
else
	$logFile = fopen(LOG_FILE, 'w');

if($logFile !== false)
{
	fwrite($logFile, $logText);
	fclose($logFile);
}

echo $logText;


?>
